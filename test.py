import sys
import unittest
from unittest.mock import MagicMock
sys.modules['SconeHwAccess.rw'] = MagicMock()
import scone
import multiprocessing as mp
import requests
import time
import re
import xmlrunner

class SconeBaseTestCase(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        cls.test_proc.terminate()

class TestSconeWithResponsiveRegisters(SconeBaseTestCase):
    @classmethod
    def setUpClass(cls):
        addresstable = 'test/address_table.json'
        cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Success'))
        cls.test_proc.start()
        time.sleep(1)

    def test_single_write_reg_available(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt/link_enable/write', data={"value": "1"})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"message":"success","status":"200","value":1}\n')

        # Should work regardless of whether we send an int or a string.
        r = requests.post('http://localhost:8080/kcu1500_ugmt/link_enable/write', data={"value": 1})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"message":"success","status":"200","value":1}\n')

    def test_aggregate_writes_reg_available(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"link_enable": "1", "reset_board": "1"})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"link_enable":{"message":"success","status":"200","value":1},"reset_board":{"message":"success","status":"200","value":1}}\n')

        # Should work regardless of whether we send an int or a string.
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"link_enable": 1, "reset_board": 1})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"link_enable":{"message":"success","status":"200","value":1},"reset_board":{"message":"success","status":"200","value":1}}\n')

    def test_aggregate_writes_reg_not_writable(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"cdr_stable_info": "1", "rx_byte_is_aligned_info": "1"})
        self.assertEqual(r.status_code, 403)
        self.assertEqual(r.text, '{"cdr_stable_info":{"message":"Bad request: register not writable","status":"403","value":-1},"rx_byte_is_aligned_info":{"message":"Bad request: register not writable","status":"403","value":-1}}\n')

        # Also make sure this works if we have a good one in there.
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"cdr_stable_info": "1", "reset_board": "1"})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"cdr_stable_info":{"message":"Bad request: register not writable","status":"403","value":-1},"reset_board":{"message":"success","status":"200","value":1}}\n')

    def test_single_read_reg_available(self):
        r = requests.get('http://localhost:8080/kcu1500_ugmt/link_enable/read')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"message":"success","status":"200","value":0}\n')

    def test_aggregate_read_reg_available(self):
        r = requests.get('http://localhost:8080/kcu1500_ugmt')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '{"cdr_stable_info":{"message":"success","status":"200","value":0},"link_enable":{"message":"success","status":"200","value":0},"reset_board":{"message":"success","status":"200","value":0},"rx_byte_is_aligned_info":{"message":"success","status":"200","value":0}}\n')

class TestSconeWithFailingRegisters(SconeBaseTestCase):
    @classmethod
    def setUpClass(cls):
        addresstable = 'test/address_table.json'
        cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Fail'))
        cls.test_proc.start()
        time.sleep(1)

    def test_single_write_reg_unavailable(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt/link_enable/write', data={"value": "1"})
        self.assertEqual(r.status_code, 500)
        self.assertEqual(r.text, '{"message":"Internal Server Error: an error occurred when trying to perform the WriteProperty function","status":"500","value":-1}\n')

    def test_aggregate_writes_reg_unavailable(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"link_enable": "1", "reset_board": "1"})
        self.assertEqual(r.status_code, 500)
        self.assertEqual(r.text, '{"link_enable":{"message":"Internal Server Error: an error occurred when trying to perform the WriteProperty function","status":"500","value":-1},"reset_board":{"message":"Internal Server Error: an error occurred when trying to perform the WriteProperty function","status":"500","value":-1}}\n')

    def test_single_read_reg_unavailable(self):
        r = requests.get('http://localhost:8080/kcu1500_ugmt/link_enable/read')
        self.assertEqual(r.status_code, 500)
        self.assertEqual(r.text, '{"message":"Internal Server Error: an error occurred when trying to perform the ReadProperty function","status":"500","value":-1}\n')

    def test_aggregate_reads_reg_unavailable(self):
        r = requests.get('http://localhost:8080/kcu1500_ugmt')
        self.assertEqual(r.status_code, 500)
        self.assertEqual(r.text, '{"cdr_stable_info":{"message":"Internal Server Error: an error occurred when trying to perform the ReadProperty function","status":"500","value":-1},"link_enable":{"message":"Internal Server Error: an error occurred when trying to perform the ReadProperty function","status":"500","value":-1},"reset_board":{"message":"Internal Server Error: an error occurred when trying to perform the ReadProperty function","status":"500","value":-1},"rx_byte_is_aligned_info":{"message":"Internal Server Error: an error occurred when trying to perform the ReadProperty function","status":"500","value":-1}}\n')

class RegisterIndependentTests(SconeBaseTestCase):
    def test_single_write_unknown_reg(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt/unknown_reg/write', data={"value": "1"})
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Bad request: register not available","status":"404","value":-1}\n')

    def test_single_write_unknown_board(self):
        r = requests.post('http://localhost:8080/unknown_board/unknown_reg/write', data={"value": "1"})
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Bad request: board not available","status":"404","value":-1}\n')

    def test_aggregate_writes_unknown_registers(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt', json={"unknown1": "1", "unknown2": "1"})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.text, '{"unknown1":{"message":"Register not found in address table","status":400,"value":0},"unknown2":{"message":"Register not found in address table","status":400,"value":0}}\n')

    def test_aggregate_writes_unknown_board(self):
        r = requests.post('http://localhost:8080/unknown_board', json={"link_enable": "1", "reset_board": "1"})
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Board not found in address table."}\n')

    def test_aggregate_writes_wrong_payload_type(self):
        r = requests.post('http://localhost:8080/kcu1500_ugmt', data={"link_enable": "1", "reset_board": "1"})
        self.assertEqual(r.status_code, 400) # Expecting failure because we're using 'data' instead of 'json'
        self.assertEqual(r.text, '{"message":"Didn\'t receive a JSON payload. Are you trying to send a form?"}\n')

    def test_single_read_unknown_reg(self):
        r = requests.get('http://localhost:8080/kcu1500_ugmt/unknown_reg/read')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Bad request: register not available","status":"404","value":-1}\n')

    def test_single_read_unknown_board(self):
        r = requests.get('http://localhost:8080/unknown_board/unknown_reg/read')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Bad request: board not available","status":"404","value":-1}\n')

    def test_aggregate_reads_unknown_board(self):
        r = requests.get('http://localhost:8080/unknown_board')
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.text, '{"message":"Board not found in address table."}\n')

    def test_prometheus(self):
        expected_prometheus_set = set(['process_start_time_seconds', 'kcu1500_ugmt_link_enable', 'process_virtual_memory_bytes', 'python_gc_objects_collected_total', 'kcu1500_ugmt_reset_board', 'kcu1500_ugmt_cdr_stable_info', 'process_resident_memory_bytes', 'process_open_fds', 'python_gc_objects_uncollectable_total', 'kcu1500_ugmt_rx_byte_is_aligned_info', 'python_gc_collections_total', 'process_cpu_seconds_total', 'python_info', 'process_max_fds'])
        r = requests.get('http://localhost:8097/')
        self.assertEqual(r.status_code, 200)
        r_set = set(re.split(r'\W+', r.text))
        self.assertTrue((r_set & expected_prometheus_set) == expected_prometheus_set)

# class TestSconeRegIndependentTestsWithResponsiveRegisters(RegisterIndependentTests):
#     @classmethod
#     def setUpClass(cls):
#         addresstable = 'test/address_table.json'
#         cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Success'))
#         cls.test_proc.start()
#         time.sleep(1)

# class TestSconeRegIndependentTestsWithFailingRegisters(RegisterIndependentTests):
#     @classmethod
#     def setUpClass(cls):
#         addresstable = 'test/address_table.json'
#         cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Fail'))
#         cls.test_proc.start()
#         time.sleep(1)


# TODO: Would be interesting if the v2 test classes can be merged with the above ones. v1 interface should still work with v2 address table. Might also be interesting to see if v1 and v2 address tables can be stored in the same file.
# v2
class TestSconeV2WithResponsiveActions(SconeBaseTestCase):
    @classmethod
    def setUpClass(cls):
        addresstable = 'test/address_table_v2.json'
        cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Success'))
        cls.test_proc.start()
        time.sleep(1)

    def test_unavailable_board(self):
        r = requests.post('http://localhost:8080/v2/unknown_board/0/unknown_action')
        self.assertEqual(r.text, '{"current_state":"Unknown","message":"Bad request: board not available","status":"404"}\n')
        self.assertEqual(r.status_code, 404)

    def test_unavailable_action(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/unknown_action')
        self.assertEqual(r.text, '{"current_state":"Configured","message":"Bad request: action not available","status":"404"}\n')
        self.assertEqual(r.status_code, 404)

    def test_reset_action(self):
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    # TODO: Why does configure take so long? (about 6-7 seconds)
    def test_configure_with_json_config(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        self.assertEqual(r.text, '{"current_state":"Configured","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_start_transition(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/start')
        self.assertEqual(r.text, '{"current_state":"Running","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_stop_transition(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/start')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/stop')
        self.assertEqual(r.text, '{"current_state":"Configured","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_halt_transition(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/start')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/stop')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/halt')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_failing_start_transition(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/start')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"While executing operation \'start\': Cannot execute START transition while in state HALTED.","status":"400"}\n')
        self.assertEqual(r.status_code, 400)

    def test_stop_from_configured_action(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/stop')
        self.assertEqual(r.text, '{"current_state":"Configured","message":"While executing operation \'stop\': Cannot execute STOP transition while in state CONFIGURED.","status":"400"}\n')
        self.assertEqual(r.status_code, 400)

    def test_configure_from_running_action(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/start')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        self.assertEqual(r.text, '{"current_state":"Running","message":"While executing operation \'configure\': Cannot execute CONFIGURE transition while in state RUNNING.","status":"400"}\n')
        self.assertEqual(r.status_code, 400)

    def test_reconfigure_from_configured_action(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        self.assertEqual(r.text, '{"current_state":"Configured","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_detailed_health_check(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.get('http://localhost:8080/v2/vcu128_ugmtbmtf/0/health/detailed')
        self.maxDiff = None
        # TODO: giving error even if strings are apparently the same
        # https://stackoverflow.com/questions/12852764/error-comparing-long-string-output-in-python-test gives a possible explanation+solution
        self.assertEqual(r.text.strip(), '{"current_state":"Configured","message":"While executing operation \'health_detailed\': Detected 55 problems: [ugmt] link clock status=0xff. [bmtf] link clock status=0xffffff. Eth clocks status=0x3. HBM clock status=0x1. PCIe AXI clock status=0x1. [ugmt] RX byte alignment value=0x1. [bmtf] RX byte alignment value=0x1. [ugmt] CDR stability value=0x1. [bmtf] CDR stability value=0x1. [bmtf] dropping orbits from output stream 0 (counter=4294967295). [bmtf] dropping orbits from output stream 1 (counter=4294967295). [bmtf] dropping orbits from output stream 2 (counter=4294967295). [bmtf] dropping orbits from output stream 3 (counter=4294967295). [bmtf] dropping orbits from output stream 4 (counter=4294967295). [bmtf] dropping orbits from output stream 5 (counter=4294967295). [bmtf] dropping orbits from output stream 6 (counter=4294967295). [bmtf] dropping orbits from output stream 7 (counter=4294967295). [bmtf] dropping orbits from output stream 8 (counter=4294967295). [bmtf] dropping orbits from output stream 9 (counter=4294967295). [bmtf] dropping orbits from output stream 10 (counter=4294967295). [bmtf] dropping orbits from output stream 11 (counter=4294967295). [ugmt] autorealign counter increasing (counter=4294967295). [bmtf] autorealign counter increasing (counter=4294967295). [ugmt] CRC check errors on input stream 0 (counter=4294967295). [ugmt] CRC check errors on input stream 1 (counter=4294967295). [ugmt] CRC check errors on input stream 2 (counter=4294967295). [ugmt] CRC check errors on input stream 3 (counter=4294967295). [ugmt] CRC check errors on input stream 4 (counter=4294967295). [ugmt] CRC check errors on input stream 5 (counter=4294967295). [ugmt] CRC check errors on input stream 6 (counter=4294967295). [ugmt] CRC check errors on input stream 7 (counter=4294967295). [bmtf] CRC check errors on input stream 0 (counter=4294967295). [bmtf] CRC check errors on input stream 1 (counter=4294967295). [bmtf] CRC check errors on input stream 2 (counter=4294967295). [bmtf] CRC check errors on input stream 3 (counter=4294967295). [bmtf] CRC check errors on input stream 4 (counter=4294967295). [bmtf] CRC check errors on input stream 5 (counter=4294967295). [bmtf] CRC check errors on input stream 6 (counter=4294967295). [bmtf] CRC check errors on input stream 7 (counter=4294967295). [bmtf] CRC check errors on input stream 8 (counter=4294967295). [bmtf] CRC check errors on input stream 9 (counter=4294967295). [bmtf] CRC check errors on input stream 10 (counter=4294967295). [bmtf] CRC check errors on input stream 11 (counter=4294967295). [bmtf] CRC check errors on input stream 12 (counter=4294967295). [bmtf] CRC check errors on input stream 13 (counter=4294967295). [bmtf] CRC check errors on input stream 14 (counter=4294967295). [bmtf] CRC check errors on input stream 15 (counter=4294967295). [bmtf] CRC check errors on input stream 16 (counter=4294967295). [bmtf] CRC check errors on input stream 17 (counter=4294967295). [bmtf] CRC check errors on input stream 18 (counter=4294967295). [bmtf] CRC check errors on input stream 19 (counter=4294967295). [bmtf] CRC check errors on input stream 20 (counter=4294967295). [bmtf] CRC check errors on input stream 21 (counter=4294967295). [bmtf] CRC check errors on input stream 22 (counter=4294967295). [bmtf] CRC check errors on input stream 23 (counter=4294967295).","status":"500"}')
        self.assertEqual(r.status_code, 500)

    # TODO: This requires some thought. Questions to answer:
    #  - Should the standard health endpoint maybe return only "healthy" or "faulty" and the detailed one give the exact reasons?
    #  - Is there something else we would want in a detailed health check?
    #  - Should we instead/in addition have an endpoint that returns the entire config file?
    # def test_detailed_health_check(self):
    #     r = requests.get('http://localhost:8080/v2/vcu128_ugmtbmtf/0/health/detailed')
    #     self.assertEqual(r.status_code, 200)
    #     self.assertEqual(r.text, '{"message":"success","status":"200", "current_state":"configured", "result":"healthy", "register_dump":""}\n')

class TestSconeV2WithFailingActions(SconeBaseTestCase):
    @classmethod
    def setUpClass(cls):
        addresstable = 'test/address_table_v2.json'
        cls.test_proc = mp.Process(target=scone.scone, args=(addresstable, 'scone.log', None, False, 15, "8080", "0.0.0.0", 'Debug', 'Always_Fail'))
        cls.test_proc.start()
        time.sleep(1)

    # TODO: Should this succeed when registers return all zero?
    def test_reset_action(self):
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"success","status":"200"}\n')
        self.assertEqual(r.status_code, 200)

    def test_available_action_with_json_config(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        self.assertEqual(r.text, '{"current_state":"Error","message":"While executing operation \'configure\': DAQ 0 sent 6 ARP requests without reply. The fiber might be disconnected or the link is not yet up.","status":"504"}\n')
        self.assertEqual(r.status_code, 504)

    def test_unavailable_action(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/unknown_action')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"Bad request: action not available","status":"404"}\n')
        self.assertEqual(r.status_code, 404)

    def test_unavailable_action_when_in_error(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/configure', json={"enabled_systems":["ugmt","bmtf"]})
        r = requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/unknown_action')
        self.assertEqual(r.text, '{"current_state":"Error","message":"Bad request: action not available","status":"404"}\n')
        self.assertEqual(r.status_code, 404)

    def test_detailed_health_check(self):
        requests.post('http://localhost:8080/v2/vcu128_ugmtbmtf/0/reset')
        r = requests.get('http://localhost:8080/v2/vcu128_ugmtbmtf/0/health/detailed')
        self.assertEqual(r.text, '{"current_state":"Halted","message":"Board has not been configured (state: fState.HALTED)","status":"200"}\n')
        self.assertEqual(r.status_code, 200)


if __name__ == '__main__':
    success = True

    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeWithResponsiveRegisters)
    success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeWithResponsiveRegisters').run(suite).wasSuccessful()
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeWithFailingRegisters)
    success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeWithFailingRegisters').run(suite).wasSuccessful()
    # suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeRegIndependentTestsWithResponsiveRegisters)
    # success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeRegIndependentTestsWithResponsiveRegisters').run(suite).wasSuccessful()
    # suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeRegIndependentTestsWithFailingRegisters)
    # success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeRegIndependentTestsWithFailingRegisters').run(suite).wasSuccessful()
    # v2
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeV2WithResponsiveActions)
    success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeV2WithResponsiveActions').run(suite).wasSuccessful()
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSconeV2WithFailingActions)
    success &= xmlrunner.XMLTestRunner(output='test_results-TestSconeV2WithFailingActions').run(suite).wasSuccessful()

    if success:
        exit(0)
    else:
        exit(1)
