# SCONE
Restful control and monitoring interface for the CMD 40MHz scouting project

## Installation
The recommended way of installation is by installing the prebuilt RPM package.

## Running
- scone.py: python interface for monitoring and control. It sets up a multiprocess manager that loops over the reading and writing requests and the monitoring request. The writing and reading requests can be sent through the web page interface (running on port 8080).

            Arguments:

            -- jsonfile (-j)   required; configuration file containing the addess table (full path)
            -- board (-b):     optional; if none given, loop over all the boards in the address table
            -- timesleep (-t): optional; monitoring rate in seconds (default = 5)
            -- logfile (-l):   optional; path to a text file to be used for logging; if not supplied, logging is sent to journald
            -- errfile (-e):   optional; path to a .txt file; if not given no logfile is created
            -- dump (-d):      optional; choose 1 to print out messages on the terminal, 0 otherwise (default = 0)

If SCONE is run as a service (and therefore is logging to journald) its logs can be retrieved with the command `journalctl -u scone [--since=-15m]`.

## Address table format
Examples of valid address tables are given in the folder `test`.
