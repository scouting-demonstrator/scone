#!/usr/bin/python3.8

import signal
import click
import json
import logging
import logging.handlers
from cysystemd import journal
import datetime
import importlib
import tracemalloc
import gc
import multiprocessing as mp
mp.set_start_method('fork')

from SconeHwAccess import rw
from SconeMonitoring import prometheus_endpoint
from SconeRestApi import server

from unittest.mock import MagicMock

logger=None

# Our main process
@click.command()
@click.option('--addresstable', '-a', type=click.Path(exists=True), help="Path to address table.", required=True)
@click.option('--logfile', '-l', help="Log file to use.")
@click.option('--board', '-b', help="Scouting board to access. (If not given all boards in address table will be accessed.)")
@click.option('--mem_debug', '-m', is_flag=True, help="Print memory tracing messages.", default=False)
@click.option('--monitoring_interval', '-i', help="Interval for reading all monitoring registers.", default=15, show_default=True)
@click.option('--port', '-p', help="Port of the web interface.", default="8080", show_default=True)
@click.option('--address', '-w', help="Address to bind the web interface to.", default="0.0.0.0", show_default=True)
@click.option('--verbosity', '-v', type=click.Choice(['Error', 'Warning', 'Info', 'Debug'], case_sensitive=False), default='Debug', show_default=True)
def main(addresstable, logfile, board, mem_debug, monitoring_interval, port, address, verbosity):
    scone(addresstable, logfile, board, mem_debug, monitoring_interval, port, address, verbosity)

# TODO: Check if we can call main directly from test script.
def scone(addresstable, logfile, board, mem_debug, monitoring_interval, port, address, verbosity, dummy_mode='Off'):
    gc.enable()
    with open(addresstable, "r") as fj:
        config_json = json.load(fj)

    if dummy_mode == 'Always_Success':
        rw.ReadProperty = MagicMock(return_value=0)
        rw.WriteProperty = MagicMock(return_value=0)
    if dummy_mode == 'Always_Fail':
        rw.ReadProperty = MagicMock(return_value=-1)
        rw.WriteProperty = MagicMock(return_value=-1)

    verbose = False
    if verbosity=="Debug":
        logLevel = logging.DEBUG
        verbose = True
    elif verbosity=="Info":
        logLevel = logging.INFO
    elif verbosity=="Warning":
        logLevel = logging.WARNING
    elif verbosity=="Error":
        logLevel = logging.ERROR
    if logfile:
        handler = logging.handlers.TimedRotatingFileHandler(logfile, when='d', interval=1, backupCount=90)
    else:
        handler = journal.JournaldLogHandler()
    logging.basicConfig(handlers=[handler], format='%(levelname)s: %(name)s: %(message)s', level=logLevel)
    logger = logging.getLogger(__name__)

    if mem_debug == True:
        tracemalloc.start()
        gc.set_debug(gc.DEBUG_LEAK)
        memory = 0

    boards = {}
    module = {}
    if board not in list(config_json.keys()) and board:
        string_boards = ""
        for b in list(config_json.keys()): string_boards +='%s '%(b)
        logger.error("Board not recognised. List of available boards: %s"%(string_boards))
        exit()
    elif board==None:
        logger.info("The argument 'board' (-b) has not been specified. The monitoring process will loop over all the boards included in the address table.")
        for b in config_json.keys():
            # this check can go away after we extend the sb852 and kcu1500 to the v2 control approach
            if "vcu128" in b:
                # TODO: unroll_register_json will be moved immediately after the json loading after changing "currently_used" dir to "currently_used/${board_index}"
                config_json[b]["registers"] = unroll_register_json(config_json[b])
                module[b] = importlib.import_module("SconeHwAccess." + b.split("_")[0])
                boards[b] = module[b].scouting_board(b, config_json[b], dummy_mode)


    q_request, q_web_response, q_moni_response = mp.Queue(), mp.Queue(), mp.Queue()
    web_proc = mp.Process(target=web_interface, args=(config_json, q_request, q_web_response, port, address))
    web_proc.daemon = True # Exit if the main process exits.
    web_proc.start()
    moni_proc = mp.Process(target=monitoring, args=(config_json, board, q_request, q_moni_response, monitoring_interval))
    moni_proc.daemon = True # Exit if the main process exits.
    moni_proc.start()

    signal.signal(signal.SIGTERM, handle_interrupt)

    try:
        while True:
            # check for reading requests
            bulk_request = q_request.get() # blocking call
            response = {}

            # check for function requests
            for request in bulk_request["requests"]:
                if "v2" in request["version"]:
                    action_request = request
                    board_to_access = action_request['board']
                    if not board_to_access in config_json:
                        ret = {'status':'404', 'message':'Bad request: board not available'}
                    elif not action_request['action'] in config_json[action_request['board']]['actions']:
                        ret = {'status':'404', 'message':'Bad request: action not available'}
                    else:
                        action          = action_request['action']
                        device          = config_json[board_to_access]["driver"]
                        json_data       = action_request['json_data']
                        action_to_run   = getattr(boards[board_to_access], action, dummy_mode)
                        ret             = action_to_run(json_data)
                        if ret['status'] != "200":
                            ret['message'] = f"While executing operation '{action}': {ret['message']}"
                        elif not ret['message']:
                            ret['message'] = 'success'
                    if action_request['board'] in config_json:
                        ret['current_state'] = boards[board_to_access].current_state.name.title()
                    else:
                        ret['current_state'] = 'Unknown'
                    response[action_request['action']] = ret

                # check for reading requests
                elif 'value' not in request:
                    read_request = request
                    status = "request received"
                    if not read_request['board'] in config_json:
                        status    = "404"
                        message   = "Bad request: board not available"
                        read_val = -1
                    elif not read_request['register'] in config_json[read_request['board']]['registers']:
                        status   = "404"
                        message  = "Bad request: register not available"
                        read_val = -1
                    else:
                        board_to_access = read_request['board']
                        register        = read_request['register']
                        device          = config_json[board_to_access]["driver"]
                        read_val        = 0
                        # this check can go away after we extend the sb852 and kcu1500 to the v2 control approach
                        if "xpci" in device:
                            try:
                                read_val = boards[board_to_access].read(register)
                            except Exception as e:
                                read_val = -1
                                logger.error("Exception during read: " + str(e))
                        else:
                            read_val    = rw.ReadProperty(device, config_json[board_to_access]["registers"][register], verbose=verbose)
                        if read_val==-1:
                            status  = "500"
                            message = 'Internal Server Error: an error occurred when trying to perform the ReadProperty function'
                        else:
                            status   = "200"
                            message  = 'success'
                    response[read_request['register']] = {'value':read_val, 'status':status, 'message':message}

                # check for writing requests
                elif 'value' in request:
                    write_request = request
                    if not write_request['board'] in config_json:
                        status    = "404"
                        message   = "Bad request: board not available"
                        write_val = -1
                    elif not write_request['register'] in config_json[write_request['board']]['registers']:
                        status    = "404"
                        message   = "Bad request: register not available"
                        write_val = -1
                    elif not config_json[write_request['board']]['registers'][write_request['register']]['writable']:
                        status    = "403"
                        message   = "Bad request: register not writable"
                        write_val = -1
                    else:
                        board_to_access = write_request['board']
                        register        = write_request['register']
                        value           = write_request['value']
                        device          = config_json[board_to_access]["driver"]
                        write_val       = 0
                        # this check can go away after we extend the sb852 and kcu1500 to the v2 control approach
                        if "xpci" in device:
                            try:
                                write_val = boards[board_to_access].write(register, value)
                            except Exception as e:
                                write_val = -1
                                logger.error("Exception during write: " + str(e))
                        else:
                            write_val   = rw.WriteProperty(device, config_json[board_to_access]["registers"][register], value)
                        if write_val==-1:
                            status  = "500"
                            message = 'Internal Server Error: an error occurred when trying to perform the WriteProperty function'
                        else:
                            status  = "200"
                            message = 'success'
                            write_val = value
                    response[write_request['register']] = {'value':write_val, 'status':status, 'message':message}

                else:
                    response = {'status':405, 'message':'Method not supported'}

            if bulk_request["source"] == "web":
                q_web_response.put(response)
            elif bulk_request["source"] == "moni":
                q_moni_response.put(response)

            if mem_debug:
                memory_avg, memory_max = tracemalloc.get_traced_memory()
                if memory_avg >memory:
                    print(datetime.datetime.now())
                    print(f"Current memory usage is {memory_avg / 10**6}MB; Peak was {memory_max / 10**6}MB")
                    memory = memory_avg

    except KeyboardInterrupt:
        web_proc.join()
        moni_proc.join()
        exit()
        tracemalloc.stop()

###### Web interface process
def web_interface(address_table, q_request, q_response, port, address):
    return server.RunApp_Flask(address_table, q_request, q_response, port=port, address=address)

###### Monitoring process
def monitoring(address_table, board, q_request, q_response, monitoring_interval):
    return prometheus_endpoint.monitor(address_table, board, q_request, q_response, monitoring_interval)

def handle_interrupt(signnum, frame):
    exit()
    tracemalloc.stop()

# unroll register dictionary
def unroll_register_json(config_json):
    reg_json = {}
    for subsystem in config_json["registers"]:
        # general registers
        if subsystem=="general":
            for reg in config_json["registers"][subsystem]:
                # check if there are multiple copies of a single register
                if "copies" in config_json["registers"][subsystem][reg].keys():
                    for ic in range(config_json["registers"][subsystem][reg].pop("copies")):
                        reg_new_name = f"{reg}_{str(ic).zfill(2)}"
                        reg_json[reg_new_name] = config_json["registers"][subsystem][reg]
                else:
                    reg_json[reg] = config_json["registers"][subsystem][reg]
        # subsystem specific registers
        else:
            for reg in config_json["registers"][subsystem]:
                # check if there are multiple copies of a single register
                if "copies" in config_json["registers"][subsystem][reg].keys():
                    for ic in range(config_json["registers"][subsystem][reg].pop("copies")):
                        reg_new_name = f"{subsystem}_{reg}_{str(ic).zfill(2)}"
                        reg_json[reg_new_name] = config_json["registers"][subsystem][reg]
                else:
                    reg_new_name = f"{subsystem}_{reg}"
                    reg_json[reg_new_name] = config_json["registers"][subsystem][reg]

    return reg_json

if __name__ == '__main__':
    main()

