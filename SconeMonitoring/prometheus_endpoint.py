import gc
import time
import logging
from multiprocessing import TimeoutError
import queue
from prometheus_client import start_http_server, Gauge, Counter

logger = logging.getLogger(__name__)

def update_metric(metric, register, metric_type, value, board, previous_read_values):
    if metric_type =='gauge':
        metric.set(value)
        return
    elif metric_type =='counter':
        previous_value = 0
        if register in previous_read_values.keys():
            previous_value = previous_read_values[register]
        if (value-previous_value)>=0:
            metric.inc(value-previous_value)
            del previous_value
            gc.collect()
            return
        else:
            # counter overflow in the firmware or board reset (the present solution is approximated)
            metric.inc(value)
            del previous_value
            gc.collect()
            logger.info('Counter %s on board %s has been reset'%(register, board))
            return
    else:
        raise Exception()

def update_history(previous_values, register, value):
    previous_values[register] = value

def monitoring_board(config_json, board, metrics, q_request, q_response, previous_read_values):
    read_requests = {}
    read_requests["source"] = "moni"
    read_requests["requests"] = []
    for reg in config_json[board]["registers"]:
        exposable = config_json[board]["registers"][reg]["exposable"]
        if not exposable: continue

        read_request = {}
        read_request["version"] = "v1"
        read_request["board"] = board
        read_request["register"] = reg
        read_requests["requests"].append(read_request)

    q_request.put(read_requests)
    try:
        # TODO: remove 1 sec timeout, since it might cause queue de-synchronizations
        # TODO: the timeout has to be handled at a lower level in the read/write operation
        # If the prometheus monitoring requests registers read while an action is issued
        # by the user/FM, the response will arrive only after the action response has been
        # obtained and the following timeout may be trigger, causing queue de-sync
        read_response = q_response.get(True, 1)     # blocking call with 1 s timeout
    except queue.Empty:
        logger.error(f"Couldn't reach main process to access registers!")
        return
    for reg, reply in read_response.items():
        if reply["status"] == "200":
            metric = config_json[board]["registers"][reg]["metric"]
            read_val = reply["value"]
            try:
                update_metric(metrics[reg], metric_type=metric, value=read_val, register=reg, board=board, previous_read_values=previous_read_values)
            except Exception as e:
                logger.error("Unknown metric: %s; available metrics are 'gauge' and 'counter'"%(e))
            update_history(previous_values=previous_read_values, register=reg, value=read_val)
        else:
            logger.error(f"Couldn't read metric {reg}.")
    gc.collect()

def monitor(config_json, board, q_request, q_response, monitoring_interval):
    # Start up the server to expose Prometheus metrics.
    start_http_server(8097)

    metrics_board_dict = {}
    previous_read_values_board = {}
    if board is None:
        for available_board in config_json:
            metrics_dict = {}
            for reg in config_json[available_board]["registers"]:
                metric   = config_json[available_board]["registers"][reg]["metric"]
                if metric == 'gauge':
                    metrics_dict[reg] = Gauge("%s_%s"%(available_board, reg), "%s_%s"%(available_board, reg))
                elif metric == 'counter':
                    metrics_dict[reg] = Counter("%s_%s"%(available_board, reg), "%s_%s"%(available_board, reg))

            metrics_board_dict[available_board]=metrics_dict
            previous_read_values_board[available_board] = {}
    else:
        metrics_dict = {}
        for reg in config_json[board]["registers"]:
            metric   = config_json[board]["registers"][reg]["metric"]
            if metric == 'gauge':
                metrics_dict[reg] = Gauge("%s_%s"%(board, reg), "%s_%s"%(board, reg))
            elif metric == 'counter':
                metrics_dict[reg] = Counter("%s_%s"%(board, reg), "%s_%s"%(board, reg))

        metrics_board_dict[board]=metrics_dict
        previous_read_values_board[board] = {}

    while True:
        if board is None:
            for available_board in config_json:
                monitoring_board(config_json, available_board, metrics_board_dict[available_board], q_request, q_response, previous_read_values_board[available_board])
        else:
            monitoring_board(config_json, board, metrics_board_dict[board], q_request, q_response, previous_read_values_board[board])

        time.sleep(monitoring_interval)

