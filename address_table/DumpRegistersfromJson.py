import json
import logging
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-j','--jsonfile', type=str, help="config file json", required=True)
    parser.add_argument('-b','--board',    type=str, help="scouting board",   required=True)
    parser.add_argument('-r','--register', type=str, help="control register", required=False)
    parser.add_argument('-f','--filelog',  type=str, help="logfile",          required=False)
    parser.add_argument('-d','--dump',     type=int ,help="dump messages",    required=False, default=1)

    args = parser.parse_args()
    if args.filelog:
        logging.basicConfig(filename=args.filelog, encoding='utf-8', level=logging.DEBUG)
    if args.dump:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    with open(args.jsonfile, 'r') as jsonfile:
        config_json = json.load(jsonfile)

    board    = args.board
    register = args.register

    if not board in config_json:
        board_list = ''
        for k in config_json.keys():
            board_list += '%s'%(k)
            if k != config_json.keys()[-1]:
                board_list +=', '
        logging.error('board %s is not defined. Avaliable boards are: %s'%(board, board_list))
        exit()
    if register != None:
        # display only the speciefied register
        if not register in config_json[board]['registers']:
            logging.error('register %s in board %s not configured.'%(register, board))
            exit()
        else:
            logging.info(config_json[board]['registers'][register])

    else:
        # loop over all the registers
        for r in config_json[board]['registers']:
            logging.info('%s: '%(r))
            logging.info(config_json[board]['registers'][r])

