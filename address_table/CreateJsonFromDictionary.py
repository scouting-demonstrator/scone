# warning: use python3.6

import json
from HandleRegisters import HandleRegisters

config_dict = { "kcu1500" : { "driver"    : "/dev/wz-xdma0_user",
                             "registers" : {}
                            }
               }

# Name, width, offset, address
monitorables = [ 
                 ('cdr_stable', 1, 0, 0x0, 'gauge', False, True),
                 ('RxByteIsAligned', 8, 1, 0x0, 'gauge', False, True),
                 ('GtPowerGood', 8, 9, 0x0, 'gauge', False, True),
                 ('GtTxResetDone', 1, 17, 0x0, 'gauge', False, False),
                 ('GtRxResetDone', 1, 18, 0x0, 'gauge', False, False),
                 ('TxPmaResetDone', 1, 19, 0x0, 'gauge', False, True),
                 ('RxPmaResetDone', 1, 20, 0x0, 'gauge', False, True),
                 ('GtResetTxPllDatapath', 1, 21, 0x0, 'gauge', True, True),
                 ('GtResetTxDatapath', 1, 22, 0x0, 'gauge', True, True),
                 ('GtResetRxDatapath', 1, 23, 0x0, 'gauge', True, True),
                 ('InitDone', 1, 24, 0x0, 'gauge', False, True),
                 ('FreqInput', 32, 0, 0x1, 'counter', False, True),
                 ('FillerOrbitsSeen', 32, 0, 0x2, 'counter', False, True),
                 ('FillerOrbitsDropped', 32, 0, 0x3, 'counter', False, True),
                 ('AxiBackpressureSeen', 1, 0, 0x4, 'gauge', False, True),
                 ('OrbitExceedsSize', 1, 1, 0x4, 'gauge', False, True),
                 ('WaitingForOrbitEnd', 1, 2, 0x4, 'counter', False, True),
                 ('I2CvalueRead', 8, 3, 0x4, 'gauge', False, True),
                 ('Autorealigns', 32, 0, 0x5, 'counter', False, True),
                 ('OrbitLength', 32, 0, 0x6, 'counter', False, True)
                 ]

if __name__ == '__main__':
    '''basic address table with 32 registers 32 bits each
    '''
    for m in monitorables:
        HandleRegisters(config_dict, action='add', board='kcu1500', register=m[0], width=m[1], offset=m[2],  address=f"0x{m[3]*4+64:x}", metric=m[4], writable=int(m[5]), exposable=int(m[6]))

    with open('address_table_kcu1500_64shift_rw.json', 'w') as outfile:
        json.dump(config_dict, outfile, indent=4)
