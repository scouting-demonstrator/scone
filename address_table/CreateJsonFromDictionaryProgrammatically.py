import json
from HandleRegisters import HandleRegisters

config_dict = { "kcu1500" : { "driver"    : "/dev/wz-xdma0_user",
                             "registers" : {}
                            }
               }

# Name, width, offset, address
monitorables = [ 
                 ('cdr_stable', 1, 0, 0x10, 'gauge'),
                 ('RxByteIsAligned', 8, 1, 0x10, 'gauge'),
                 ('GtPowerGood', 8, 9, 0x10, 'gauge'),
                 ('GtTxResetDone', 1, 17, 0x10, 'gauge'),
                 ('GtRxResetDone', 1, 18, 0x10, 'gauge'),
                 ('TxPmaResetDone', 1, 19, 0x10, 'gauge'),
                 ('RxPmaResetDone', 1, 20, 0x10, 'gauge'),
                 ('GtResetTxPllDatapath', 1, 21, 0x10, 'gauge'),
                 ('GtResetTxDatapath', 1, 22, 0x10, 'gauge'),
                 ('GtResetRxDatapath', 1, 23, 0x10, 'gauge'),
                 ('InitDone', 1, 24, 0x10, 'gauge'),
                 ('FreqInput', 32, 0, 0x11, 'gauge'),
                 ('FillerOrbitsSeen', 32, 0, 0x12, 'counter'),
                 ('FillerOrbitsDropped', 32, 0, 0x13, 'counter'),
                 ('AxiBackpressureSeen', 1, 0, 0x14, 'gauge'),
                 ('OrbitExceedsSize', 1, 1, 0x14, 'gauge'),
                 ('WaitingForOrbitEnd', 1, 2, 0x14, 'gauge'),
                 ('I2CvalueRead', 8, 3, 0x14, 'gauge'),
                 ('Autorealigns', 32, 0, 0x15, 'counter'),
                 ('OrbitLength', 32, 0, 0x16, 'gauge')
                 ]

if __name__ == '__main__':
    '''basic address table with 32 registers 32 bits each
    '''
    for m in monitorables:
        HandleRegisters(config_dict, action='add', board='kcu1500', register=m[0], width=m[1], offset=m[2], address=f"0x{m[3]*4:x}", metric=m[4])

    with open('address_table_kcu1500.json', 'w') as outfile:
        json.dump(config_dict, outfile, indent=4)
