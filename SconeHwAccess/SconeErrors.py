class SconeError(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.http_code = "500"

class SconeTransitionError(SconeError):
    def __init__(self, transition, current_state):
        super().__init__(f"Cannot execute {transition} transition while in state {current_state}.")
        self.http_code = "400"

class SconeUnknownParameterError(SconeError):
    def __init__(self, unknown_paramter):
        super().__init__(f"Trying to modify unknown parameter {unknown_paramter}.")
        self.http_code = "400"

class SconeIoError(SconeError):
    def __init__(self, message):
        super().__init__(message)
        self.http_code = "503"

class SconeTimeoutError(SconeError):
    def __init__(self, message):
        super().__init__(message)
        self.http_code = "504"