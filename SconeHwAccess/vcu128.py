import os
import sys
import json
from enum import Enum, auto
import time
import json
import logging
import signal
import functools

from unittest.mock import MagicMock
from SconeHwAccess.SconeErrors import *

logger = logging.getLogger(__name__)

# From https://stackoverflow.com/a/75928879
def timeout(seconds=1, default=None):

    def decorator(func):

        @functools.wraps(func)
        def wrapper(*args, **kwargs):

            def handle_timeout(signum, frame):
                raise SconeTimeoutError("Operation timed out")

            signal.signal(signal.SIGALRM, handle_timeout)
            signal.setitimer(signal.ITIMER_REAL, seconds, 0)

            result = func(*args, **kwargs)

            signal.setitimer(signal.ITIMER_REAL, 0, 0)

            return result

        return wrapper

    return decorator

# inspired by
# https://gitlab.cern.ch/dth_p1-v2/DTH_Software/-/blob/master/DTH_Control/pylib/dthlib.py
class scouting_board():
    """
    A class with high and low level functionalities for the vcu128.
    The class uses the PyHAL PCIDevice to handle the board registers access
    """

    class State(Enum):
        HALTED = auto()
        CONFIGURED = auto()
        RUNNING = auto()
        ERROR = auto()

    def __init__( self, board_name, json_config, dummy_mode="Off" ):
        """
        The constructor of the vcu128 class:
        + it reads a json file with the board configuration
        + it instantiates a PyHAL PCIDevice object
        """
        # get board name and json configuration
        self.dummy_mode = dummy_mode
        self.json_config = json_config
        self.board_name = board_name
        self.board_index = json_config["index"]
        self.board_vendorId = int(json_config["vendorId"], 16)
        self.board_deviceId = int(json_config["deviceId"], 16)
        self.hal_addrtable = json_config["hal_addrtable"]
        self.hal_addrpath = json_config["hal_addrpath"]
        self.rw_timeout_sec = 0.5 # TODO: this value is hardcoded for now

        # initialization of device interfaces
        self.device = None
        self.i2c_GPIO     = None
        self.i2c_switch_1 = None
        self.i2c_switch_2 = None
        self.i2c_qsfp     = None
        self.i2c_fmcp     = None
        self.i2c_eeprom   = None

        # retrieve configuration from savestate file (if application crashed and was restarted)
        self.board_config_savestate = f"{self.hal_addrpath}address_table_{self.board_name}_{self.board_index}_savestate.json"
        if os.path.exists(self.board_config_savestate):
            fjson = open(self.board_config_savestate)
            self.json_config = json.load(fjson)
            fjson.close()

        # if not in in test mode
        if self.dummy_mode=="Off":
            from hal import PCIDevice
            from SconeHwAccess.I2CInterface import I2CInterface

            # instantiate PCI device for xpci driver
            self.device = PCIDevice((json_config["hal_addrpath"] + json_config["hal_addrtable"]),
                                    self.board_vendorId, self.board_deviceId, self.board_index)

            # enable the PCI interface in case this was not done by the BIOS
            self.enable_card(self.device)

            # Initialise the i2c interfaces of the vcu128
            i2c_items = {'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}
            self.i2c_GPIO     = I2CInterface(self.device, 0x40, offsetWidth=0x1, dataWidth=0x1, items=i2c_items, debugFlag=True)
            self.i2c_switch_1 = I2CInterface(self.device, 0xE8, offsetWidth=0x0, dataWidth=0x1, items=i2c_items, debugFlag=True)
            self.i2c_switch_2 = I2CInterface(self.device, 0xEC, offsetWidth=0x0, dataWidth=0x1, items=i2c_items, debugFlag=True)
            self.i2c_qsfp     = I2CInterface(self.device, 0xBA, offsetWidth=0x1, dataWidth=0x1, items=i2c_items, debugFlag=True)
            self.i2c_fmcp     = I2CInterface(self.device, 0xEE, offsetWidth=0x1, dataWidth=0x1, items=i2c_items, debugFlag=True)
            self.i2c_eeprom   = I2CInterface(self.device, 0xA8, offsetWidth=0x1, dataWidth=0x1, items=i2c_items, debugFlag=True)

        # store control parameters and firmware module offsets
        self.ctrl_pars  = self.json_config["parameters"]
        self.offset     = self.json_config["module_offsets"]

        # Some basic constants and parameters
        self.enabled_systems = self.ctrl_pars['enabled_systems']
        self.l1ds_pipelines = self.ctrl_pars['l1ds_pipelines']

        self.daq_enable = self.ctrl_pars['daq_enable']
        self.tcp_stream_enable = self.ctrl_pars['tcp_stream_enable']
        self.tcp_daq_map = self.ctrl_pars["tcp_daq_map"]
        self.tcp_source_port = self.ctrl_pars['sourcePort']
        self.tcp_dest_port = self.ctrl_pars['destPort']

        self.source_IP = self.ctrl_pars['sourceIp']
        self.dest_IP = self.ctrl_pars['destIp']
        self.tcp_stream_per_daq = []
        for i in range(len(self.daq_enable)):
            self.tcp_stream_per_daq.append(self.tcp_daq_map.count(i))

        # enable/disable daq/tcp depending on l1ds pipeline enabled/disabled
        # TODO: some logic here might be simplified, since "enabled_system" and "l1ds_pipelines"/src/"enable" are redundant
        for src in self.l1ds_pipelines.keys():
            if src in self.enabled_systems:
                self.l1ds_pipelines[src]["enable"] = 1
                logger.info(f"{src} pipeline is enabled")
                # enable daq
                for idaq in self.l1ds_pipelines[src]["daq_map"]:
                    self.daq_enable[idaq] = 1
                    logger.info(f"    Enabling DAQ #{idaq}")
                # enable tcp
                for itcp in self.l1ds_pipelines[src]["tcp_stream_map"]:
                    self.tcp_stream_enable[itcp] = 1
                    logger.info(f"    Enabling TCP #{itcp}")
            else:
                self.l1ds_pipelines[src]["enable"] = 0
                logger.info(f"{src} pipeline is disabled")
                # disable daq
                for idaq in self.l1ds_pipelines[src]["daq_map"]:
                    self.daq_enable[idaq] = 0
                    logger.info(f"    Disabling DAQ #{idaq}")
                # disable tcp
                for itcp in self.l1ds_pipelines[src]["tcp_stream_map"]:
                    self.tcp_stream_enable[itcp] = 0
                    logger.info(f"    Disabling TCP #{itcp}")

        # TCP parameters
        self.tcp_params = { "eth_100gb_tcp_win"                   : self.ctrl_pars["eth_100gb_tcp_win"],
                            "eth_100gb_ctrl_scale_def"            : self.ctrl_pars["eth_100gb_ctrl_scale_def"],
                            "eth_100gb_ctrl_mss_def_def"          : self.ctrl_pars["eth_100gb_ctrl_mss_def_def"],
                            "eth_100gb_ctrl_timestamp_def"        : self.ctrl_pars["eth_100gb_ctrl_timestamp_def"],
                            "eth_100gb_ctrl_timestamp_reply_def"  : self.ctrl_pars["eth_100gb_ctrl_timestamp_reply_def"],
                            "eth_100gb_ctrl_congwind_def"         : self.ctrl_pars["eth_100gb_ctrl_congwind_def"],
                            "eth_100gb_ctrl_timer_rtt_def"        : self.ctrl_pars["eth_100gb_ctrl_timer_rtt_def"],
                            "eth_100gb_ctrl_timer_rtt_sync_def"   : self.ctrl_pars["eth_100gb_ctrl_timer_rtt_sync_def"],
                            "eth_100gb_ctrl_timer_persist_def"    : self.ctrl_pars["eth_100gb_ctrl_timer_persist_def"],
                            "eth_100gb_ctrl_thresold_retrans_def" : self.ctrl_pars["eth_100gb_ctrl_thresold_retrans_def"],
                            "eth_100gb_ctrl_rexmt_cwnd_sh_def"    : self.ctrl_pars["eth_100gb_ctrl_rexmt_cwnd_sh_def"], }

        # Offsets
        self.offset_hbm = self.offset["hbm_item"]
        self.offset_tcp = self.offset["tcp_stream"]
        self.offset_eth = self.offset["daq_eth_100gb"]

        # initialize counters for health check
        self.packager_dropped_orbits = []
        self.autorealigns_total = []
        self.crc_error_counter = []
        for src in self.l1ds_pipelines.keys():
            self.packager_dropped_orbits.append([0] * len(self.l1ds_pipelines[src]["tcp_stream_map"]))
            self.autorealigns_total.append(0)
            self.crc_error_counter.append([0] * self.l1ds_pipelines[src]["nInputLinks"])

        # If we don't get the current state from the configuration (or the saved state), we default to ERROR
        self.current_state = self.State[self.json_config.get("board_status", "ERROR")]

        # mock up class methods given by json file
        if self.dummy_mode == 'Always_Success':
            self.read      = MagicMock(return_value=0xFFFFFFFF)
            self.read64    = MagicMock(return_value=0xFFFFFFFFFFFFFFFF)
            self.write     = MagicMock(return_value=0)
            self.write64   = MagicMock(return_value=0)
        if self.dummy_mode == 'Always_Fail':
            # TODO: might be good to add side_effect to raise exceptions?
            self.read      = MagicMock(return_value=0) # TODO: We don't get failures when read returns -1. Should check that.
            self.read64    = MagicMock(return_value=0)
            self.write     = MagicMock(return_value=-1) # TODO: Return value of write doesn't seem to be checked. (Update: Because it throws and exception if it fails.)
            self.write64   = MagicMock(return_value=-1)



    def enable_card( self, device ):
        """
        Function to enable the PyHAL PCIDevice
        """
        PCIe_enabled = device.read("PCIe_Command", 0x0)

        if (PCIe_enabled & 0x1)==0:
            device.write("PCIe_Command", 0x1, False, 0x0)
            logger.info("PCIe memory access needed enable")



    def update_board_config(self, json_data={}):
        """
        Function to update the current json configuration file,
        which was given by the class constructor parameter
        """

        message, status = "", "200"
        logger.info("Update board configuration")

        try:
            for k in json_data.keys():
                if k in self.json_config["parameters"].keys():
                    if k=="l1ds_pipelines":
                        for src in json_data[k].keys():
                            if src in self.l1ds_pipelines.keys():
                                for subk in json_data[k][src].keys():
                                    if subk in self.json_config["parameters"][k][src].keys():
                                        old_entry = self.json_config["parameters"][k][src][subk]
                                        new_entry = json_data[k][src][subk]
                                        logger.info(f"Updating {k}/{src}/{subk} (old: {old_entry}, new: {new_entry})")
                                        self.json_config["parameters"][k][src][subk] = json_data[k][src][subk]
                                    else:
                                        raise SconeUnknownParameterError(subk)
                            else:
                                raise SconeUnknownParameterError(src)
                    else:
                        old_entry = self.json_config["parameters"][k]
                        new_entry = json_data[k]
                        logger.info(f"Updating {k} (old: {old_entry}, new: {new_entry})")
                        self.json_config["parameters"][k] = json_data[k]
                else:
                    raise SconeUnknownParameterError(k)

            # reload parameters
            self.ctrl_pars  = self.json_config["parameters"]

            self.enabled_systems = self.ctrl_pars['enabled_systems']
            self.daq_enable = self.ctrl_pars['daq_enable']
            self.tcp_stream_enable = self.ctrl_pars['tcp_stream_enable']
            self.tcp_source_port = self.ctrl_pars['sourcePort']
            self.tcp_dest_port = self.ctrl_pars['destPort']
            self.source_IP = self.ctrl_pars['sourceIp']
            self.dest_IP = self.ctrl_pars['destIp']

            self.tcp_params = { "eth_100gb_tcp_win"                   : self.ctrl_pars["eth_100gb_tcp_win"],
                                "eth_100gb_ctrl_scale_def"            : self.ctrl_pars["eth_100gb_ctrl_scale_def"],
                                "eth_100gb_ctrl_mss_def_def"          : self.ctrl_pars["eth_100gb_ctrl_mss_def_def"],
                                "eth_100gb_ctrl_timestamp_def"        : self.ctrl_pars["eth_100gb_ctrl_timestamp_def"],
                                "eth_100gb_ctrl_timestamp_reply_def"  : self.ctrl_pars["eth_100gb_ctrl_timestamp_reply_def"],
                                "eth_100gb_ctrl_congwind_def"         : self.ctrl_pars["eth_100gb_ctrl_congwind_def"],
                                "eth_100gb_ctrl_timer_rtt_def"        : self.ctrl_pars["eth_100gb_ctrl_timer_rtt_def"],
                                "eth_100gb_ctrl_timer_rtt_sync_def"   : self.ctrl_pars["eth_100gb_ctrl_timer_rtt_sync_def"],
                                "eth_100gb_ctrl_timer_persist_def"    : self.ctrl_pars["eth_100gb_ctrl_timer_persist_def"],
                                "eth_100gb_ctrl_thresold_retrans_def" : self.ctrl_pars["eth_100gb_ctrl_thresold_retrans_def"],
                                "eth_100gb_ctrl_rexmt_cwnd_sh_def"    : self.ctrl_pars["eth_100gb_ctrl_rexmt_cwnd_sh_def"], }

            # enable/disable daq/tcp depending on l1ds pipeline enabled/disabled
            # TODO: some logic here might be simplified, since "enabled_system" and "l1ds_pipelines"/src/"enable" are redundant
            for src in self.l1ds_pipelines.keys():
                if src in self.enabled_systems:
                    self.l1ds_pipelines[src]["enable"] = 1
                    logger.info(f"{src} pipeline is enabled")
                    # enable daq
                    for idaq in self.l1ds_pipelines[src]["daq_map"]:
                        self.daq_enable[idaq] = 1
                        logger.info(f"    Enabling DAQ #{idaq}")
                    # enable tcp
                    for itcp in self.l1ds_pipelines[src]["tcp_stream_map"]:
                        self.tcp_stream_enable[itcp] = 1
                        logger.info(f"    Enabling TCP #{itcp}")
                else:
                    self.l1ds_pipelines[src]["enable"] = 0
                    logger.info(f"{src} pipeline is disabled")
                    # disable daq
                    for idaq in self.l1ds_pipelines[src]["daq_map"]:
                        self.daq_enable[idaq] = 0
                        logger.info(f"    Disabling DAQ #{idaq}")
                    # disable tcp
                    for itcp in self.l1ds_pipelines[src]["tcp_stream_map"]:
                        self.tcp_stream_enable[itcp] = 0
                        logger.info(f"    Disabling TCP #{itcp}")

            logger.info("Board configuration updated successfully")
            return {'status':"200", 'message':"success"}

        except SconeUnknownParameterError as e:
            logger.error(f"During board configuration update: {e}")
            raise
        except Exception as e:
            logger.error(f"Exception during board configuration update: {e}")
            raise SconeError(f"Unknown exception occured during board configuration update: {e}")


    def dump_board_config(self, json_data={}):
        """
        Function to dump to the log the current configuration
        """

        message, status = "", "200"
        logger.info("Dump board configuration")

        try:
            for k in self.json_config["parameters"].keys():
                entry = self.json_config["parameters"][k]
                logger.info("" + k + ": " + str(entry))

            logger.info("Board configuration dumped successfully")
            return {'status':"200", 'message':"success"}

        except Exception as e:
            logger.error(f"Exception during dump board configuration: {e}")
            raise SconeError(f"Unknown exception occured while trying to dump board config: {e}")


    def dump_board_config_savestate(self, json_data={}):
        """
        Function to dump a json save state of the board config.
        Needed to recover the board config if the application
        crashes while running
        """

        self.json_config["board_status"] = self.current_state.name

        message, status = "", "200"
        logger.info("Dump board configuration to json savestate")

        try:
            with open(self.board_config_savestate, 'w') as f:
                json.dump(self.json_config, f)

            logger.info("Board configuration savestate dumped successfully")
            return {'status':"200", 'message':"success"}

        except Exception as e:
            logger.error(f"Exception during dump board configuration savestate: {e}")
            raise SconeError(f"Unknown exception occured while trying to dump board savestate: {e}")


    def delete_board_config_savestate(self, json_data={}):
        """
        Function to delete a json save state of the board config.
        Needed when halt() is called, so that the board restarts
        from a default state
        """

        message, status = "", "200"
        logger.info("Delete board configuration savestate")

        try:
            if os.path.exists(self.board_config_savestate):
                os.remove(self.board_config_savestate)

            logger.info("Board configuration savestate deleted successfully")
            return {'status':status, 'message':message}

        except Exception as e:
            logger.error(f"Exception during delete of board configuration savestate: {e}")
            raise SconeError(f"Unknown exception occured while trying to delete board savestate: {e}")

    @timeout(seconds=1)
    def read(self, register, offset=0, json_data={}):
        """
        Function to perform a 32-bit register read
        """
        try:
            value = self.device.read(register, offset)
            return value
        except SconeTimeoutError as e:
            raise e
        except:
            raise SconeIoError("Failed read")

    @timeout(seconds=1)
    def read64(self, register, offset=0, json_data={}):
        """
        Function to perform a 64-bit register read.
        Some registers have 64-bit length, so this function allows to read
        them with a single read call rather than 2 x 32-bit read calls
        """
        try:
            value = self.device.read64(register, offset)
            return value
        except SconeTimeoutError as e:
            raise e
        except:
            raise SconeIoError("Failed read64")

    @timeout(seconds=1)
    def write(self, register, value, check=False, offset=0, json_data={}):
        """
        Function to write to a 32-bit register
        """
        try:
            self.device.write(register, value, check, offset)
        except SconeTimeoutError as e:
            raise e
        except:
            raise SconeIoError("Failed write")

    @timeout(seconds=1)
    def write64(self, register, value, check=False, offset=0, json_data={}):
        """
        Function to write to a 64-bit register
        Some registers have 64-bit length, so this function allows to write
        to them with a single write call rather than 2 x 32-bit write calls
        """
        try:
            self.device.write64(register, value, check, offset)
        except SconeTimeoutError as e:
            raise e
        except:
            raise SconeIoError("Failed write64")

    def reset(self, json_data={}):
        """
        Function to reset all the modules of the vcu128 board
        """

        message, status = "", "200"
        logger.info("Start of reset")

        try:
            # be sure input links are disabled to avoid breaking HBM logic
            self.disable_input_links()

            # generic reset
            self.write64("generic_reset",1)

            # reset counters
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    self.write64("eth_100gb_counter_reset", 0x1, False, self.offset_eth[i])
                    self.write64("eth_100gb_counter_reset", 0x0, False, self.offset_eth[i])
            for i, enable in enumerate(self.tcp_stream_enable):
                if enable:
                    self.write64("tcp_100gb_counter_reset", 0x1, False, self.offset_tcp[i])
                    self.write64("tcp_100gb_counter_reset", 0x0, False, self.offset_tcp[i])

            time.sleep(0.1) # TODO: check if this is not enough

            # reset I2C (QSFP, clock, ...)
            reset = self.read64("reset_hw_comp")
            reset_wr = reset or 0xff010000
            self.write64("reset_hw_comp", reset_wr, False)
            time.sleep(0.1)
            reset_wr = reset and not(0xff010000)
            self.write64("reset_hw_comp", reset_wr, False)

            # I2C reset
            reset_wr = reset or 0x80000000
            self.write64("reset_hw_comp", reset_wr, False)
            time.sleep(0.1)
            reset_wr = reset and not(0x80000000)
            self.write64("reset_hw_comp", reset_wr, False)

            # reset scouting modules
            for src in self.l1ds_pipelines.keys():
                if self.l1ds_pipelines[src]["enable"]==1:
                    self.write64(f"{src}_local_reset", 0x1, False)
                    time.sleep(0.1)
                    self.write64(f"{src}_local_reset", 0x0, False)

            # reset HBM
            ret = self.reset_hbm()

            self.current_state = self.State.HALTED
            logger.info("reset done")
            return {'status':"200", 'message':"success"}

        except SconeIoError as e:
            message, status = str(e), e.http_code
        except SconeTimeoutError as e:
            message, status = str(e), e.http_code
        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        self.current_state = self.State.ERROR
        logger.error(f"Exception during reset: {message}")
        return {'status':status, 'message':message}


    def configure(self, json_data={}):
        """
        Function to configure vcu128 board. Main modules:
        - Scouting firmware (alignment, ZS, merge, packager)
        - HBM write/read
        - TCP logic and MAC 100GbE
        """

        message, status = "", "200"

        try:
            if self.current_state != self.State.HALTED and self.current_state != self.State.CONFIGURED:
                raise SconeTransitionError("CONFIGURE", self.current_state.name)

            logger.info("Starting board configure")

            # update json config
            ret = self.update_board_config(json_data)

            # setup DAQ transceivers
            ret = self.configure_daq_transceiver()

            # reset hbm
            ret = self.reset_hbm()

            # reset counters
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    self.write64("eth_100gb_counter_reset", 0x1, False, self.offset_eth[i])
                    self.write64("eth_100gb_counter_reset", 0x0, False, self.offset_eth[i])
            for i, enable in enumerate(self.tcp_stream_enable):
                if enable:
                    self.write64("tcp_100gb_counter_reset", 0x1, False, self.offset_tcp[i])
                    self.write64("tcp_100gb_counter_reset", 0x0, False, self.offset_tcp[i])

            # IP, tcp parameters, analog DAQ signal
            ret = self.configure_ip_pars()
            ret = self.configure_tcp_pars()
            ret = self.configure_daq_serdes()

            # enable scouting algo
            ret = self.enable_stream_reshape()

            # remap trigger input links
            ret = self.remap_trigger_input_links()

            # ARP request
            ret = self.send_arp()

            logger.info("Saving current state.")
            ret = self.dump_board_config_savestate()

            self.current_state = self.State.CONFIGURED
            logger.info("Configure done.")
            return {'status':"200", 'message':"success"}

        except SconeTransitionError as e: # This one doesn't send us into ERROR state.
            message, status = str(e), e.http_code
        except SconeTimeoutError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except SconeError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except Exception as e:
            self.current_state = self.State.ERROR
            message, status = str(e), "500"
        # We only get here if any of the exceptions fired, but not if we succeeded.
        logger.error(f"Exception during configure: {message}")
        return {'status':status, 'message':message}


    def start(self, json_data={}):
        """
        Function to start the data acquisition from vcu128. In particular:
        - connect board to receiving units
        - enable the trigger input links
        """

        message, status = "", "200"

        try:
            if self.current_state != self.State.CONFIGURED:
                raise SconeTransitionError("START", self.current_state.name)

            logger.info(f"Starting scouting board {self.board_name}")

            # open connection
            ret = self.open_connection()
            time.sleep(1.0)

            # enable links
            ret = self.enable_input_links()

            ret = self.dump_board_config_savestate()

            self.current_state = self.State.RUNNING
            logger.info("start done")
            return {'status':"200", 'message':"success"}

        except SconeTransitionError as e: # This one doesn't send us into ERROR state.
            message, status = str(e), e.http_code
        except SconeTimeoutError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except SconeError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except Exception as e:
            self.current_state = self.State.ERROR
            message, status = str(e), "500"
        logger.error(f"Exception during DAQ start: {message}")
        return {'status':status, 'message':message}


    def stop(self, json_data={}):
        """
        Function to stop the data acquisition from vcu128. In particular:
        - disable the trigger input links
        - close the connection between the board and the receiving units
        """

        message, status = "", "200"

        try:
            if self.current_state != self.State.RUNNING:
                raise SconeTransitionError("STOP", self.current_state.name)

            logger.info("Stopping DAQ")

            # mask trigger input links request...
            ret = self.disable_input_links()
            # ...and let data after aligner flush to the HBM
            time.sleep(1.0)

            # close connection
            ret = self.close_connection()

            ret = self.dump_board_config_savestate()

            self.current_state = self.State.CONFIGURED
            logger.info("stop done")
            return {'status':"200", 'message':"success"}

        except SconeTransitionError as e: # This one doesn't send us into ERROR state.
            message, status = str(e), e.http_code
        except SconeTimeoutError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except SconeError as e:
            self.current_state = self.State.ERROR
            message, status = str(e), e.http_code
        except Exception as e:
            self.current_state = self.State.ERROR
            message, status = str(e), "500"
        logger.error(f"Exception during DAQ stop: {message}")
        return {'status':status, 'message':message}


    def halt(self, json_data={}):
        """
        Function to halt the data acquisition from vcu128. In particular:
        - disable the trigger input links
        - close the connection between the board and the receiving units
        """

        message, status = "", "200"
        logger.info("Halting DAQ")

        try:
            # delete json config savestate
            self.delete_board_config_savestate()

            # mask trigger input links request...
            self.disable_input_links()
            # ...and let data after aligner flush to the HBM
            time.sleep(1.0)

            # close connection
            self.close_connection()

            # reset
            ret = self.reset()

            if ret['status'] == "200":
                self.current_state = self.State.HALTED
                logger.info("halt done")

            return ret

        except SconeTimeoutError as e:
            message, status = str(e), e.http_code
        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        self.current_state = self.State.ERROR
        logger.error(f"Exception during DAQ halt: {message}")
        return {'status':status, 'message':message}



    def health_input(self, json_data={}):
        """
        Function to check the health of the vcu128 input links
        """

        message, status = "", "200"
        logger.info("Input links health check")

        try:
            message_list = []
            if (self.current_state == self.State.CONFIGURED) or (self.current_state == self.State.RUNNING):
                # input links health check
                logger.info("Rx byte alignment health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        rx_byte_is_aligned_status = 0
                        check = "rx_byte_is_aligned_info"
                        res = self.read(f"{src}_rx_byte_is_aligned_info")
                        if res != 2**(4*self.l1ds_pipelines[src]["nRegions"])-1:
                            rx_byte_is_aligned_status = 1
                            message_list.append(f"[{src}] RX byte alignment value={hex(rx_byte_is_aligned_status)}.")

                logger.info("CDR stable health check")
                check = "cdr_stable_info"
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        cdr_stable_status = 0
                        res = self.read(f"{src}_cdr_stable_info")
                        if res != 2**(self.l1ds_pipelines[src]["nRegions"])-1:
                            cdr_stable_status = 1
                            message_list.append(f"[{src}] CDR stability value={hex(cdr_stable_status)}.")

                if message_list:
                    message = f"Detected {len(message_list)} problems: " + " ".join(message_list)
                    status = "500"
                else:
                    message = "healthy"

                return {'status':status, 'message':message}

            else:
                message = f"Board has not been configured (state: f{self.current_state})"
                status = "200"

                return {'status':status, 'message':message}

        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        logger.error(f"Exception during board input links health check: {message}")
        return {'status':status, 'message':message}



    def health_clock(self, json_data={}):
        """
        Function to check the health of the vcu128 clocks
        """

        message, status = "", "200"
        logger.info("Clocks health check")

        try:
            message_list = []
            if (self.current_state == self.State.CONFIGURED) or (self.current_state == self.State.RUNNING):
                # link clocks health check
                logger.info("Link clocks health check")
                link_clock_status = 0
                check = "freq_clk_rec"
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        for i in range(self.l1ds_pipelines[src]["nInputLinks"]):
                            meas = self.read(f"{src}_freq_clk_rec_00", i*8)
                            if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                                link_clock_status = link_clock_status | (1 << i)
                        if link_clock_status != 0:
                            message_list.append(f"[{src}] link clock status={hex(link_clock_status)}.")

                # ethernet clocks health check
                logger.info("Ethernet clocks health check")
                eth_clock_status = 0
                check = "freq_clk_eth"
                for i, enable in enumerate(self.daq_enable):
                    if enable:
                        meas = self.read("clock_eth_measure", self.offset_eth[i])
                        if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                            eth_clock_status = eth_clock_status | (1 << i)
                if eth_clock_status != 0:
                    message_list.append(f"Eth clocks status={hex(eth_clock_status)}.")

                # hbm clock health check
                logger.info("HBM clock health check")
                hbm_clock_status = 0
                check = "freq_clk_hbm"
                meas = self.read("clock_sr0_measure")
                if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                    hbm_clock_status = 1
                    message_list.append(f"HBM clock status={hex(hbm_clock_status)}.")

                # axi clock health check
                logger.info("PCIe AXI clock health check")
                axi_clock_status = 0
                check = "freq_clk_axi"
                meas = self.read("freq_clk_axi")
                if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                    axi_clock_status = 1
                    message_list.append(f"PCIe AXI clock status={hex(axi_clock_status)}.")

                if message_list:
                    message = f"Detected {len(message_list)} problems: " + " ".join(message_list)
                    status = "500"
                else:
                    message = "healthy"

                return {'status':status, 'message':message}

            else:
                message = f"Board has not been configured (state: f{self.current_state})"
                status = "200"

                return {'status':status, 'message':message}

        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        logger.error(f"Exception during board clocks health check: {message}")
        return {'status':status, 'message':message}



    def health_data(self, json_data={}):
        """
        Function to check the health of the vcu128 data streams
        """

        message, status = "", "200"
        logger.info("Data streams health check")

        try:
            message_list = []
            if (self.current_state == self.State.CONFIGURED) or (self.current_state == self.State.RUNNING):
                # dropped orbits check
                logger.info("Packager dropped orbits health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        for i, enable in enumerate(self.l1ds_pipelines[src]["tcp_stream_map"]):
                            if enable:
                                meas = self.read(f"{src}_packager_dropped_orbits_00", i*8)
                                if meas > self.packager_dropped_orbits[sIdx][i]:
                                    self.packager_dropped_orbits[sIdx][i] = meas
                                    message_list.append(f"[{src}] dropping orbits from output stream {i} (counter={meas}).")

                # autorealign counter health check
                logger.info("Autorealign counts health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        meas = self.read(f"{src}_autorealigns_total")
                        if meas > self.autorealigns_total[sIdx]:
                            self.autorealigns_total[sIdx] = meas
                            message_list.append(f"[{src}] autorealign counter increasing (counter={meas}).")

                # crc error counters health check
                logger.info("Input CRC error counts health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        for i in range(self.l1ds_pipelines[src]["nInputLinks"]):
                            meas = self.read(f"{src}_crc_error_counter_00", i*8)
                            if meas > self.crc_error_counter[sIdx][i]:
                                self.crc_error_counter[sIdx][i] = meas
                                message_list.append(f"[{src}] CRC check errors on input stream {i} (counter={meas}).")

                if message_list:
                    message = f"Detected {len(message_list)} problems: " + " ".join(message_list)
                    status = "500"
                else:
                    message = "healthy"

                return {'status':status, 'message':message}

            else:
                message = f"Board has not been configured (state: f{self.current_state})"
                status = "200"

                return {'status':status, 'message':message}

        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        logger.error(f"Exception during board data streams health check: {message}")
        return {'status':status, 'message':message}



    def health_detailed(self, json_data={}):
        """
        Function to check the health of the vcu128. In particular:
        - check the link health
        - close the connection between the board and the receiving units
        """

        message, status = "", "200"
        logger.info("Health check")

        try:
            message_list = []
            if (self.current_state == self.State.CONFIGURED) or (self.current_state == self.State.RUNNING):
                # link clocks health check
                logger.info("Link clocks health check")
                link_clock_status = 0
                check = "freq_clk_rec"
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        for i in range(self.l1ds_pipelines[src]["nInputLinks"]):
                            meas = self.read(f"{src}_freq_clk_rec_00", i*8)
                            if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                                link_clock_status = link_clock_status | (1 << i)
                        if link_clock_status != 0:
                            message_list.append(f"[{src}] link clock status={hex(link_clock_status)}.")

                # ethernet clocks health check
                logger.info("Ethernet clocks health check")
                eth_clock_status = 0
                check = "freq_clk_eth"
                for i, enable in enumerate(self.daq_enable):
                    if enable:
                        meas = self.read("clock_eth_measure", self.offset_eth[i])
                        if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                            eth_clock_status = eth_clock_status | (1 << i)
                if eth_clock_status != 0:
                    message_list.append(f"Eth clocks status={hex(eth_clock_status)}.")

                # hbm clock health check
                logger.info("HBM clock health check")
                hbm_clock_status = 0
                check = "freq_clk_hbm"
                meas = self.read("clock_sr0_measure")
                if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                    hbm_clock_status = 1
                    message_list.append(f"HBM clock status={hex(hbm_clock_status)}.")

                # axi clock health check
                logger.info("PCIe AXI clock health check")
                axi_clock_status = 0
                check = "freq_clk_axi"
                meas = self.read("freq_clk_axi")
                if (meas - self.json_config["health_check"][check]["expected_val"]) > 10000:
                    axi_clock_status = 1
                    message_list.append(f"PCIe AXI clock status={hex(axi_clock_status)}.")

                # input links health check
                logger.info("Rx byte alignment health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        rx_byte_is_aligned_status = 0
                        check = "rx_byte_is_aligned_info"
                        res = self.read(f"{src}_rx_byte_is_aligned_info")
                        if res != 2**(4*self.l1ds_pipelines[src]["nRegions"])-1:
                            rx_byte_is_aligned_status = 1
                            message_list.append(f"[{src}] RX byte alignment value={hex(rx_byte_is_aligned_status)}.")

                logger.info("CDR stable health check")
                check = "cdr_stable_info"
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        cdr_stable_status = 0
                        res = self.read(f"{src}_cdr_stable_info")
                        if res != 2**(self.l1ds_pipelines[src]["nRegions"])-1:
                            cdr_stable_status = 1
                            message_list.append(f"[{src}] CDR stability value={hex(cdr_stable_status)}.")

                # dropped orbits check
                logger.info("Packager dropped orbits health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        for i, enable in enumerate(self.l1ds_pipelines[src]["tcp_stream_map"]):
                            if enable:
                                meas = self.read(f"{src}_packager_dropped_orbits_00", i*8)
                                if meas > self.packager_dropped_orbits[sIdx][i]:
                                    self.packager_dropped_orbits[sIdx][i] = meas
                                    message_list.append(f"[{src}] dropping orbits from output stream {i} (counter={meas}).")

                # autorealign counter health check
                logger.info("Autorealign counts health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        meas = self.read(f"{src}_autorealigns_total")
                        if meas > self.autorealigns_total[sIdx]:
                            self.autorealigns_total[sIdx] = meas
                            message_list.append(f"[{src}] autorealign counter increasing (counter={meas}).")

                # crc error counters health check
                logger.info("Input CRC error counts health check")
                for src in self.l1ds_pipelines.keys():
                    if self.l1ds_pipelines[src]["enable"]==1:
                        sIdx = self.l1ds_pipelines[src]["sourceIdx"]
                        for i in range(self.l1ds_pipelines[src]["nInputLinks"]):
                            meas = self.read(f"{src}_crc_error_counter_00", i*8)
                            if meas > self.crc_error_counter[sIdx][i]:
                                self.crc_error_counter[sIdx][i] = meas
                                message_list.append(f"[{src}] CRC check errors on input stream {i} (counter={meas}).")

                if message_list:
                    message = f"Detected {len(message_list)} problems: " + " ".join(message_list)
                    status = "500"
                else:
                    message = "healthy"

                return {'status':status, 'message':message}

            else:
                message = f"Board has not been configured (state: f{self.current_state})"
                status = "200"

                return {'status':status, 'message':message}

        except SconeError as e:
            message, status = str(e), e.http_code
        except Exception as e:
            message, status = str(e), "500"
        logger.error(f"Exception during board health check: {message}")
        return {'status':status, 'message':message}


    def remap_trigger_input_links(self, json_data={}):
        """
        Function to re-map the input links.
        Example: "input_link_map" : [1,3,0,2, ...]
        + FPGA stream #0 -> trigger input link #1
        + FPGA stream #1 -> trigger input link #3
        + FPGA stream #2 -> trigger input link #0
        + FPGA stream #3 -> trigger input link #2
        + ...
        """

        message, status = "", "200"
        logger.info("Remapping trigger input links")

        try:
            for src in self.l1ds_pipelines.keys():
                if self.l1ds_pipelines[src]["enable"]==1:
                    input_link_map = self.l1ds_pipelines[src]["input_link_map"]
                    for i in range(len(input_link_map)):
                        self.write(f"{src}_link_map_" + str(i).zfill(2), input_link_map[i])

            logger.info("Trigger input links remapped successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Trigger input links remap failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Trigger input links remap failed: {e}")
            raise SconeError(f"Unknown exception occured during input links remap: {e}")


    def enable_input_links(self, json_data={}):
        """
        Function to enable trigger input links
        """

        message, status = "", "200"
        logger.info("Enabling trigger input links")

        try:
            for src in self.l1ds_pipelines.keys():
                if self.l1ds_pipelines[src]["enable"]==1:
                    mask = 2**self.l1ds_pipelines[src]["nInputLinks"] - 1
                    self.write64(f"{src}_stream_enable_mask", mask)
            logger.info("Trigger input links enabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Trigger input links enable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Trigger input links enable failed: {e}")
            raise SconeError(f"Unknown exception occured while enabling input links: {e}")


    def disable_input_links(self, json_data={}):
        """
        Function to enable trigger input links
        """

        message, status = "", "200"
        logger.info("Disabling trigger input links")

        try:
            for src in self.l1ds_pipelines.keys():
                self.write64(f"{src}_stream_enable_mask", 0x0)
            logger.info("Trigger input links disabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Trigger input links disable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Trigger input links disable failed: {e}")
            raise  SconeError(f"Unknown exception occured while disabling input links: {e}")


    def enable_data_gen(self, json_data={}):
        """
        Function to enable internal data generator
        and bypass trigger input links
        """

        message, status = "", "200"
        logger.info("Enabling internal data generator")

        try:
            for src in self.l1ds_pipelines.keys():
                self.write64(f"{src}_enable_data_gen", 0x1)
            logger.info("Internal generator enabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Internal generator enable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Internal generator enable failed: {e}")
            raise SconeError(f"Unknown exception occured while enabling internal data generator: {e}")


    def disable_data_gen(self, json_data={}):
        """
        Function to disable internal data generator
        and bypass trigger input links
        """

        message, status = "", "200"
        logger.info("Disabling internal data generator")

        try:
            for src in self.l1ds_pipelines.keys():
                self.write64(f"{src}_enable_data_gen", 0x0)
            logger.info("Internal generator disabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Internal generator disable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Internal generator disable failed: {e}")
            raise SconeError(f"Unknown exception occured while disabling internal data generator: {e}")


    def enable_stream_reshape(self, json_data={}):
        """
        Function to enable scouting stream reshape, namely:
        - rearranging 2 links BX data in a 256b x 2 frames block
        """

        message, status = "", "200"
        logger.info("Enabling scouting stream reshape")

        try:
            for src in self.l1ds_pipelines.keys():
                self.write64(f"{src}_disable_reshape", 0x0)
            logger.info("Scouting stream reshape enabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Scouting stream reshape enable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Scouting stream reshape enable failed: {e}")
            raise SconeError(f"Unknown exception occured while enabling the stream reshape: {e}")


    def disable_stream_reshape(self, json_data={}):
        """
        Function to disable stream reshape
        """

        message, status = "", "200"
        logger.info("Disabling scouting stream reshape")

        try:
            for src in self.l1ds_pipelines.keys():
                self.write64(f"{src}_disable_reshape", 0x1)
            logger.info("Scouting stream reshape disabled successfully")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Scouting stream reshape disable failed: {e}")
            raise
        except Exception as e:
            logger.error(f"Scouting stream reshape disable failed: {e}")
            raise SconeError(f"Unknown exception occured while disabling the stream reshape: {e}")


    def configure_daq_transceiver(self, json_data={}):
        """
        Function to configure the transceivers and QSFPs used
        for the 100 GbE output
        """

        message, status = "", "200"
        logger.info("Configure DAQ serdes IP and QSFP")

        try:
            # double generic reset
            self.write64("generic_reset", 1)
            self.write64("generic_reset", 1)

            # reset the Ultra RAMs
            self.write64("reset_hw_comp", 0x08000001)
            time.sleep(0.1) # TODO: check if this is not enough
            self.write64("reset_hw_comp", 0x00000000)

            # reset the SR receiver counters
            self.write64("reset_hw_comp", 0x00010000)
            time.sleep(0.1) # TODO: check if this is not enough
            self.write64("reset_hw_comp", 0x00000000)


            # set serdes analog with default value
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    self.write64("mac100g_serdes_voltage", 0x200a0218, False, self.offset_eth[i])

            # enable QSFP
            self.write64("qsfp_monitoring", 0x02020203)
            time.sleep(0.1) # TODO: check if this is not enough
            qsfp_ena = self.read64("qsfp_monitoring", 0)
            message += "QSFP enable : " + hex(qsfp_ena) + "\n"


            # reset serdes MAC100
            message += "Reset SERDES" + "\n"
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    self.write64("mac100g_serdes_reset", 0x80000003, False, self.offset_eth[i])
                    time.sleep(0.1)
                    self.write64("mac100g_serdes_reset", 0x00000000, False, self.offset_eth[i])

            logger.info("DAQ serdes IP and QSFP configured correctly")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.error(f"Exception during configure DAQ serdes IP: {e}")
            raise
        except Exception as e:
            logger.error(f"Exception during configure DAQ serdes IP: {e}")
            raise SconeError(f"Unknown exception occured while configuring DAQ transceiver: {e}")


    def reset_hbm(self, json_data={}):
        """
        Function to reset the HBM controller
        and the wr/rd pointers
        """

        message, status = "", "200"
        logger.info("Resetting HBM")

        try:
            reset = self.read64("reset_hw_comp")
            reset_hbm = reset or (0x00020000)
            self.write64("reset_hw_comp", reset_hbm, False, 0)

            time.sleep(0.5) # TODO: check if this is not enough
            reset_hbm = reset and not(0x00020000)
            self.write64("reset_hw_comp", reset_hbm, False, 0)

            logger.info("HBM reset successful")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.info(f"HBM reset failed: {e}")
            raise
        except Exception as e:
            logger.info(f"HBM reset failed: {e}")
            raise SconeError(f"Unknown exception occured during HBM reset: {e}")


    def configure_ip_pars(self, json_data={}):
        """
        Function to write to the device the source and
        destination IPs, as well as the stream number
        on each MAC100
        """

        message, status = "", "200"
        logger.info("Writing IP parameters to device")

        try:
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    # set the source IP number
                    IP_number = self.source_IP[i].split(".")
                    sIP = int(IP_number[0]) * 0x1000000 + int(IP_number[1]) * 0x10000 + int(IP_number[2]) * 0x100 + int(IP_number[3])
                    self.write64("eth_100gb_ctrl_ips_def", sIP, False, self.offset_eth[i])
                    logger.info("    DAQ " + str(i) + " source IP: " + self.source_IP[i])

                    # set the destination IP number
                    IP_number = self.dest_IP[i].split(".")
                    dIP = int(IP_number[0]) * 0x1000000 + int(IP_number[1]) * 0x10000 + int(IP_number[2]) * 0x100 + int(IP_number[3])
                    self.write64("eth_100gb_ctrl_ipd_def", dIP, False, self.offset_eth[i])
                    logger.info("    DAQ " + str(i) + " destination IP: " + self.dest_IP[i])

            logger.info("IP parameters written to device")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.info(f"IP parameters not written to device: {e}")
            raise
        except Exception as e:
            logger.info(f"IP parameters not written to device: {e}")
            raise SconeError(f"Unknown exception occured while writing IP parameters to device: {e}")


    def configure_tcp_pars(self, json_data={}):
        """
        Write standard TCP parameters to device
        and write source and destination ports
        """

        message, status = "", "200"
        logger.info("Writing standard TCP parameters and src/dest ports to device")

        try:
            # set the tcp ports
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    for j in range(0, self.tcp_stream_per_daq[i]):
                        # Specify the number of tcp logic for each DAQ 100 Gb/s
                        self.write64("tcp_100gb_ctrl_tcp_stream", j+1, False, self.offset_tcp[sum(self.tcp_stream_per_daq[:i])+j])

                        # Set tcp ports Source and Destination
                        tcp_ports = (self.tcp_source_port[i][j]) * 0x10000 + (self.tcp_dest_port[i][j])
                        self.write64("eth_100gb_ctrl_port0_def", tcp_ports, False, self.offset_eth[i] + (8*j))

            logger.info("Written standard TCP parameters and src/dest ports to device")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.info(f"Standard TCP parameters and src/dest ports write to device failed: {e}")
            raise
        except Exception as e:
            logger.info(f"Standard TCP parameters and src/dest ports write to device failed: {e}")
            raise SconeError(f"Unknown exception occured while writing TCP parameters to device: {e}")


    def configure_daq_serdes(self, json_data={}):
        """
        Function to set standard analog parameters
        for output QSFPs.
        """

        message, status = "", "200"
        logger.info("Setting standard analog parameters for DAQ links")

        ## these parameters are set by experiment on QSFP and DTH_p1-v2 board
        tx_diff_ctrl    = 24
        tx_post_cur     = 0
        tx_pre_cur      = 0

        try:
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    reg_val = 0x20000000 + (int(tx_pre_cur) * 0x10000) + (int(tx_post_cur) * 0x100) + int(tx_diff_ctrl)
                    self.write64("mac100g_serdes_voltage", reg_val, False, self.offset_eth[i])

            # wait for the analog signal to be set correctly
            # removing completely this wait might cause problems in subsequent ARP request
            time.sleep(0.1) # TODO: check if this is not enough

            logger.info("Successfully set standard analog parameters for DAQ links")
            return {'status':status, 'message':message}

        except SconeError as e:
            logger.info("Standard analog parameters for DAQ links configure failed: {e}")
            raise
        except Exception as e:
            logger.info("Standard analog parameters for DAQ links configure failed: {e}")
            raise SconeError(f"Unknown exception occured while configuring DAQ serdes: {e}")


    def send_arp(self, json_data={}):
        """
        Function used to send ARP packets to receiving side.
        """

        message, status = "", "200"
        logger.info("Sending ARPs to receiver unit")

        try:
            arp_fail = 0
            for i, enable in enumerate(self.daq_enable):
                if enable:
                    logger.info(f"Sending ARP request for DAQ {i}")
                    self.write64("eth_100gb_eth_request_status", 0x1, False, self.offset_eth[i])

                    nb_arp_req = 0
                    nb_arp_check = 0
                    while True:
                        arp_done = self.read64("eth_100gb_eth_request_status", self.offset_eth[i])
                        nb_arp_check += 1
                        if arp_done & 0x10000 != 0:
                            # Check if the MAC address received is available not NULL
                            if self.read64("eth_100gb_dest_mac_address", self.offset_eth[i]) == 0:
                                logger.error(f"The MAC address of the destination is not set for DAQ {i}.")
                                raise SconeError(f"The MAC address of the destination is not set for DAQ {i}")
                            break
                        # If the reply is not received after a while, we resend the ARP request.
                        elif nb_arp_check > 10:
                            logger.warn(f"Retrying ARP request for DAQ {i}")
                            nb_arp_req += 1
                            if nb_arp_req > 5:
                                arp_fail = arp_fail | (1 << i)
                                logger.error(f"DAQ {i} sent {nb_arp_req} ARP requests without reply. The fiber might be disconnected or the link is not yet up.")
                                raise SconeTimeoutError(f"DAQ {i} sent {nb_arp_req} ARP requests without reply. The fiber might be disconnected or the link is not yet up.")
                            nb_arp_check = 0
                        self.write64("eth_100gb_eth_request_status", 0x1, False, self.offset_eth[i])
                        time.sleep(0.1)

                    # check for conflicting IPs/MACs
                    # from
                    # https://gitlab.cern.ch/cms_daq_dth/dth_daq_controller/-/blob/master/DTH_control.py
                    if self.dummy_mode=="Off":
                        logger.info(f"Checking for conflicting IPs/MACs for DAQ {i}")
                        mac_conflict       = self.read64("eth_100gb_MAC_Conflict", self.offset_eth[i])
                        ip_conflict        = self.read64("eth_100gb_IP_Conflict", self.offset_eth[i])
                        ip_of_mac_conflict = self.read64("eth_100gb_MAC_WITH_IP_CONFLICT", self.offset_eth[i])
                        if mac_conflict != 0:
                            logger.error(f"There is a conflict with a duplicated MAC address for DAQ {i}")
                            logger.error(f"This IP has the same MAC address: {hex(ip_of_mac_conflict)}")
                            raise SconeError(f"Duplicated MAC address conflict for DAQ {i} (IP with same MAC address: {hex(ip_of_mac_conflict)})")
                        if ip_conflict != 0:
                            logger.error(f"There is a conflict with a duplicated IP address for DAQ {i}")
                            raise SconeError(f"There is a conflict with a duplicated IP address for DAQ {i}")

            if arp_fail == 0:
                logger.info("Success.")
                return {'status':"200", 'message':"success"}
            else:
                raise SconeTimeoutError(f"Sending ARPs to receiver unit failed. Failure code: {bin(arp_fail)}")

        except SconeTimeoutError as e:
            logger.warning(f"Sending ARPs to receiver units failed: {e}")
            raise
        except SconeError as e:
            logger.warning(f"Sending ARPs to receiver units failed: {e}")
            raise
        except Exception as e:
            logger.warning(f"Sending ARPs to receiver units failed: {e}")
            raise SconeError(f"Unknown exception occured while sending ARPs to receiver units: {e}")


    def open_connection(self, json_data={}):
        """
        Function to open connection with receiving unit.
        It loops along all the enabled TCP streams and
        opens them one by one.
        """

        message, status = "", "200"
        logger.info("Opening connection with receiving units")

        try:
            # code: full status code (1 = closed, 2 = waiting, 4 = open)
            # bits: summary for all streams
            tcp_state_code = [0] * len(self.tcp_stream_enable)
            tcp_error_code = 0

            # loop over all streams...
            for i, tcp_enable in enumerate(self.tcp_stream_enable) :

                # ...and select only the enabled ones from enabled DAQ units
                if self.daq_enable[self.tcp_daq_map[i]]==1 and tcp_enable:

                    # request to open the connection
                    logger.info(f"    Opening connection port {i} from DAQ {self.tcp_daq_map[i]}")
                    data = 0x10
                    self.write64("tcp_100gb_eth_request_status", data, False, self.offset_tcp[i])

                    nb_tcp_state_read = 0
                    check = 0
                    while check==0:
                        tcp_state_code[i] = self.read64("tcp_100gb_tcp_state", self.offset_tcp[i])
                        nb_tcp_state_read = nb_tcp_state_read + 1

                        # check if the tcp state is "OPEN"
                        if tcp_state_code[i] & 0x4 != 0 :
                            check = 1
                            logger.info(f"    Connection {i} open after {nb_tcp_state_read} status read checks")

                        # if the reply is not received after a while, there is a problem
                        elif nb_tcp_state_read > 50:
                            check = 1
                            message += "TCP #" + str(i) + "\n"
                            logger.warn("        Open connection timeout: " + str(nb_tcp_state_read))

                            # connection still closed
                            if tcp_state_code[i] == 1:
                                message += "    Connection is still closed" + "\n"
                                logger.warn("        Connection is still closed")
                                tcp_error_code = tcp_error_code | (1 << 4*i)
                            # connection waiting to be opened
                            elif tcp_state_code[i] == 2:
                                message += "    Connection is waiting to be opened, try again" + "\n"
                                logger.warn("        Connection is waiting to be opened, try again")
                                tcp_error_code = tcp_error_code | (2 << 4*i)

                            message += "    TCP state is: " + str(tcp_state_code[i]) + "\n"
                            message += "    Perhaps the receiving software is not running" + "\n"
                            logger.warn("        TCP state is: " + str(tcp_state_code[i]))
                            logger.warn("        Perhaps the receiving software is not running")

                            # do not try to continue opening connections if one failed
                            raise SconeError(f"Failed to open TCP connection for stream {i}. Failure state {bin(tcp_error_code)}")

                        time.sleep(0.01)

            status = tcp_error_code
            if tcp_error_code!=0:
                raise SconeError("Failed to open TCP connection. Failure state " + bin(tcp_error_code))
            else:
                logger.info("Successfully opened connection with receiving units")
                return {'status':"200", 'message':"success"}

        except SconeError as e:
            logger.warning(f"Couldn't open connection to receiving units: {e}")
            raise
        except Exception as e:
            logger.warning(f"Open connection with receiving units failed: {e}")
            raise SconeError(f"Unknown exception occured while opening connections to the receiving units: {e}")


    def close_connection(self, json_data={}):
        """
        Function to close connection with receiving unit.
        It loops along all the enabled TCP streams and
        closes them one by one.
        """

        message, status = "", "200"
        logger.info("Closing connection with receiving units")

        try:
            for i, tcp_enable in enumerate(self.tcp_stream_enable) :
                if self.daq_enable[self.tcp_daq_map[i]]==1 and tcp_enable:
                    # request to close the connection
                    logger.info("    Closing connection port: " + str(i))
                    self.write64("tcp_100gb_eth_request_status", 0x20, False, self.offset_tcp[i])

            logger.info("Successfully closed connection with receiving units")
            return {'status':"200", 'message':"success"}

        except SconeError as e:
            logger.warning(f"Couldn't close connection to receiving units: {e}")
            raise
        except Exception as e:
            logger.warning(f"Closing connection with receiving units failed: {e}")
            raise SconeError(f"Unknown exception occured while closing connections to the receiving units: {e}")
