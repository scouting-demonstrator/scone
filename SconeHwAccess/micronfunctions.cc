#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <byteswap.h>
#include <string.h>
#include <iostream>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/mman.h>

#include <micron_dma.h>

extern "C" int main(){
  return 0;
}

extern "C" uint32_t read_bits_micron(int addr, uint32_t *value, int verbose){
  //create the picodriver object and attach to installed driver 
  micron_private* pico_;

  int errpico = micron_find_pico_sb852(true, &pico_);
  int count = 0;
  while((count < 20) && (errpico < 0)){
    sleep(0.001);
    errpico = micron_find_pico_sb852(true, &pico_);
    count ++;
  }
  if(errpico < 0){
    printf("<4> WARNING: no pico found",errpico);
    return -1;
  }
  int err;  
  //read register
  try{
    err = micron_ReadDeviceAbsolute(pico_, addr, value, 4);//fixed to read 32b at a time
  }catch(...){
    printf("<3> ERROR: micron_ReadDeviceAbsolute failed with err: %i",err);
    return -1;
  }
  printf("<7> read value: %x %x \n",*value,"at address",addr);
  micron_close_fd(pico_);

  return 0;  
}

extern "C" uint32_t write_bits_micron(int addr, uint32_t writeval, int verbose){
  
  //create the picodriver object and attach to installed driver 
  micron_private* pico_;
  int errpico = micron_find_pico_sb852(true, &pico_);
  int count = 0;
  while((count < 20) && (errpico < 0)){
    sleep(0.001);
    errpico = micron_find_pico_sb852(true, &pico_);
    count ++;
  }
  if(errpico < 0){
    printf("<4> WARNING: no pico found",errpico);
    return -1;
  }

  int err;
  //write to register
  try{
  err = micron_WriteDeviceAbsolute(pico_, addr, &writeval, 4);//fixed to write 32b at a time
  } catch(...) {
    printf("<3> ERROR: micron_WriteDeviceAbsolute failed with %i",err);
    return -1;
  }
  
  if(err!=4){
    printf("<4> WARNING: number of bytes read from pico bus transaction != 4"); 
    return -1;
  }

  printf("<7> written value: %x %s %x \n",writeval,"to address",addr);
  micron_close_fd(pico_);

  return 0;
}

