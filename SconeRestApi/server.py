import logging
import flask
import queue

from gevent.pywsgi import WSGIServer
from gevent.pool import Pool

logger = logging.getLogger(__name__)

def RunApp_Flask(address_table, q_request, q_response, port="8080",address="0.0.0.0"):
    app = flask.Flask(__name__)

    @app.route('/<board>/<register>/<action>', methods=['GET'])
    def manage_actions(board, register, action):
        if action=='read':
            logger.debug('Read request on %s %s'%(board, register))
            request = {}
            request["source"] = "web"
            request["requests"] = [{'version':'v1', 'board':board, 'register':register}]
            q_request.put(request)
            try:
                # TODO: remove 1 sec timeout, since it might cause queue de-synchronizations
                # TODO: the timeout has to be handled at a lower level in the read/write operation
                read_response = q_response.get(True, 1)     # blocking call with 1 s timeout
            except queue.Empty:
                message="Couldn't reach main process for accessing the board."
                return {'message': message}, 504
            read_request_get = read_response[register]
            value = read_request_get['value']
            status = read_request_get['status']
            message = read_request_get['message']
            logger.debug(f"Got result of read request: {read_request_get}")
            return read_request_get, read_request_get['status']

    @app.route('/<board>/<register>/<action>', methods=['POST'])
    def back_from_writing(board, register, action):
        value = flask.request.form['value']
        if action=='write':
            logger.debug(f"Write request on: {board} {register} {value}")
            request = {}
            request["source"] = "web"
            request["requests"] = [{'version':'v1', 'board':board, 'register':register, 'value':int(value)}]
            q_request.put(request)
            try:
                # TODO: remove 1 sec timeout, since it might cause queue de-synchronizations
                # TODO: the timeout has to be handled at a lower level in the read/write operation
                write_response = q_response.get(True, 1)    # blocking call with 1 s timeout
            except queue.Empty:
                message="Couldn't reach main process for accessing the board."
                return {'message': message}, 504
            write_request_get = write_response[register]
            status = write_request_get['status']
            message= write_request_get['message']
            logger.debug(f"Got result of write request: {write_request_get}")
            return write_request_get, write_request_get['status']

    @app.route('/<board>', methods=['GET'])
    def get_json(board):
        if not board in address_table:
            return {'message': "Board not found in address table."}, 404
        requests = {}
        requests["source"] = "web"
        requests["requests"] = []
        for register in address_table[board]['registers']:
            requests["requests"].append({'version':'v1', 'board':board, 'register':register})

        q_request.put(requests)
        try:
            # TODO: remove 1 sec timeout, since it might cause queue de-synchronizations
            # TODO: the timeout has to be handled at a lower level in the read/write operation
            read_response = q_response.get(True, 1)     # blocking call with 1 s timeout
        except queue.Empty:
            return {'message': 'Communication with register access application lost.'}, 504

        http_error_code = 200
        for reg, return_dict in read_response.items():
            status = int(return_dict["status"])
            # If at least one of the items in our return dict has status 200 we return that. Otherwise we return the highest error code we have.
            # Note: Using the highest error code is debatable, however it's probably useful because it causes us to get the blame first (and therefore react quickly).
            if status == 200:
                return read_response, 200
            elif status > http_error_code:
                http_error_code = status
        return read_response, http_error_code

    @app.route('/<board>', methods=['POST'])
    def post_json(board):
        json_list = flask.request.get_json()
        if not json_list:
            return {'message': "Didn't receive a JSON payload. Are you trying to send a form?"}, 400
        if not board in address_table:
            return {'message': "Board not found in address table."}, 404
        requests = {}
        requests["source"] = "web"
        requests["requests"] = []
        fail_registers = {}
        for register, value in json_list.items():
            if register in address_table[board]['registers']:
                requests["requests"].append({'version':'v1', 'board':board, 'register':register, 'value':int(value)})
            else:
                fail_registers[register] = {'value':0, 'status':400, 'message':'Register not found in address table'}

        q_request.put(requests)
        try:
            # TODO: remove 1 sec timeout, since it might cause queue de-synchronizations
            # TODO: the timeout has to be handled at a lower level in the read/write operation
            write_response = q_response.get(True, 1)    # blocking call with 1 s timeout
        except queue.Empty:
            return {'message': "Couldn't reach main process for accessing the board."}, 504

        write_response.update(fail_registers)
        http_error_code = 200
        for reg, return_dict in write_response.items():
            status = int(return_dict["status"])
            # If at least one of the items in our return dict has status 200 we return that. Otherwise we return the highest error code we have.
            # Note: Using the highest error code is debatable, however it's probably useful because it causes us to get the blame first (and therefore react quickly).
            if status == 200:
                return write_response, 200
            elif status > http_error_code:
                http_error_code = status
        return write_response, http_error_code

    @app.route('/v2/<board>/<index>/health/<check>', methods=['GET'])
    def get_health(board, index, check):
        health_check = f"health_{check}"
        logger.debug(f"health/{check} request on: {board} {index}")
        request = {}
        request["source"] = "web"
        request["requests"] = [{'version':'v2', 'board':board, 'index':index, 'action':health_check, 'json_data':{}}]
        q_request.put(request)
        try:
            action_response = q_response.get(True)
        except queue.Empty:
            message = "Couldn't reach main process for accessing the board."
            return {'message': message}, 504
        action_request_get = action_response[health_check]
        logger.debug(f"Got result of health/{check} request: {action_request_get}")
        return action_request_get, action_request_get['status']

    @app.route('/v2/<board>/<index>/<action>', methods=['POST'])
    def ctrl_action(board, index, action):
        json_data = flask.request.json

        logger.debug(f"Action request on: {board} {index} {action}")
        request = {}
        request["source"] = "web"
        request["requests"] = [{'version':'v2', 'board':board, 'index':index, 'action':action, 'json_data':json_data}]
        q_request.put(request)
        try:
            action_response = q_response.get(True)
        except queue.Empty:
            message = "Couldn't reach main process for accessing the board."
            return {'message': message}, 504
        action_request_get = action_response[action]
        logger.debug(f"Got result of action request: {action_request_get}")
        return action_request_get, action_request_get['status']

    try:
        pool = Pool(10)
        http_server = WSGIServer((address, int(port)), app, spawn=pool, log=logger)
        http_server.serve_forever()
    except KeyboardInterrupt:
        return

