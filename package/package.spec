%define _prefix  /opt/l1scouting-scone
%define _systemd /etc/systemd/system
%define _homedir %{_prefix}/scone
%define debug_package %{nil}

# To skip bytecompilation
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

Name: cms-scone
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting Hardware Access Layer API
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: %{name}.tar
ExclusiveOs: linux
Provides: cms-scone
Requires: python38,scouting-hardware-tools,openssl,cms-libmicron-devel,systemd-devel

Prefix: %{_prefix}

%description
CMS L1 Scouting Hardware Access Layer API application with included Prometheus endpoint.

%files
%defattr(-,scouter,root,-)
%attr(-, scouter, root) %dir %{_prefix}
%attr(-, scouter, root) %dir %{_prefix}/logs
%attr(-, scouter, root) %dir %{_homedir}
%attr(-, scouter, root) %{_homedir}/*
%attr(644, root, root) %config %{_systemd}/scone.service

%prep
%setup -c

%build
cd scone/SconeHwAccess
make all

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
pwd
tar cf - . | (cd  $RPM_BUILD_ROOT%{_prefix}; tar xfp - )
mkdir -p $RPM_BUILD_ROOT%{_systemd}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/logs
cd $RPM_BUILD_ROOT%{_homedir}/address_table/
mv $RPM_BUILD_ROOT%{_homedir}/systemd/scone.service $RPM_BUILD_ROOT%{_systemd}

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :

%pre

# Upgrading
if [ $1 -gt 1 ] ; then
  systemctl stop scone
  systemctl daemon-reload
fi

%post

# Register the service and start it
systemctl daemon-reload
systemctl enable --now scone

%preun

# Stopping service before removal
if [ $1 -eq 0 ] ; then
  systemctl stop scone
fi

%postun

# Only for uninstall!
if [ $1 -eq 0 ] ; then

  # Removing folder
  systemctl daemon-reload
  rm -fR %{_homedir}
  rmdir --ignore-fail-on-non-empty %{_prefix}/logs
  rmdir --ignore-fail-on-non-empty %{_prefix}

fi
